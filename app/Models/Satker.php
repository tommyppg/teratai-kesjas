<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Satker extends Model
{
    use HasFactory;

    public $timestamps = true;

    protected $fillable = [
        'nama_satker',
        'tipe',
        'created_at',
        'updated_at'
    ];  
}
