<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PoliDituju extends Model
{
    use HasFactory;

    protected $fillable = [
        'id_rikkes',
        'id_tindakan',
        'id_jenis_poli',
        'status',
        'created_at',
        'updated_at'
    ];
}
