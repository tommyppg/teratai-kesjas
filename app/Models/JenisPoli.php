<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JenisPoli extends Model
{
    use HasFactory;

    protected $fillable = [
        'nama_jenis_poli',
        'created_at',
        'updated_at'
    ];
}
