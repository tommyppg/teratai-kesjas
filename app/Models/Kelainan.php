<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kelainan extends Model
{
    use HasFactory;

    protected $fillable = [
        'id_rikkes',
        'deskripsi',
        'id_daftar_kelainan',
        'created_at',
        'updated_at'
    ];  
}
