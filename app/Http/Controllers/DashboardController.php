<?php
 
namespace App\Http\Controllers;
 
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Rikkes;
use App\Models\TKJ;
use App\Models\Kelainan;
use Illuminate\Support\Facades\DB;
use App\Mail\TerataikesjasEmail;
use Illuminate\Support\Facades\Mail;
 
class DashboardController extends Controller
{
    /**
     * Show the profile for a given user.
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function index($tahun = null)
    {   
        //count data aktivasi user
        $dataAktivasiUser['active'] = DB::table('users')->where(['password_status' => 'complete', 'profile_status' => 'complete', 'role' => 'anggota'])->count();
        $dataAktivasiUser['inactive'] = DB::table('users')->where(['password_status' => 'uncomplete', 'profile_status' => 'uncomplete', 'role' => 'anggota'])->count();

        //count data rikkes
        $dataCountSummary['countAll'] = DB::table('rikkes')->count();
        $dataCountSummary['countIntensif1'] = DB::table('rikkes')->where("intensif", "INTENSIF 1")->count();
        $dataCountSummary['countIntensif2'] = DB::table('rikkes')->where("intensif", "INTENSIF 2")->count();
        $dataCountSummary['countIntensif3'] = DB::table('rikkes')->where("intensif", "INTENSIF 3")->count();

        //grafik nilai rikkes
        $dataGraphNilai['countNilaiB'] = DB::table('rikkes')->where("kualitas", "BAIK (B)")->count();
        $dataGraphNilai['countNilaiC'] = DB::table('rikkes')->where("kualitas", "CUKUP (C)")->count();
        $dataGraphNilai['countNilaiK1'] = DB::table('rikkes')->where("kualitas", "KURANG (K1)")->count();
        $dataGraphNilai['countNilaiK2'] = DB::table('rikkes')->where("kualitas", "KURANG SEKALI (K2)")->count();

        //kelainan
        $dataAllKelainan = DB::table('kelainans')->select('nama_kelainan', DB::raw('count(*) as total'))->join('daftar_kelainans', 'daftar_kelainans.id', '=', 'kelainans.id_daftar_kelainan')->groupBy('nama_kelainan')->get();

        $topKelainan = DB::table('kelainans')->select('nama_kelainan', DB::raw('count(*) as total'))->join('daftar_kelainans', 'daftar_kelainans.id', '=', 'kelainans.id_daftar_kelainan')->groupBy('nama_kelainan')->orderBy('total', 'desc')->skip(0)->take(10)->get();

        $dataGraphKelainan['all'] = $dataAllKelainan;
        $dataGraphKelainan['topKelainan'] = $topKelainan;

        //TKJ
        //grafik nilai rikkes
        $dataGraphNilaiTKJ['countNilaiBS'] = DB::table('tkj')->where("kategori", "BS")->count();
        $dataGraphNilaiTKJ['countNilaiB'] = DB::table('tkj')->where("kategori", "B")->count();
        $dataGraphNilaiTKJ['countNilaiC'] = DB::table('tkj')->where("kategori", "C")->count();
        $dataGraphNilaiTKJ['countNilaiKS'] = DB::table('tkj')->where("kategori", "KS")->count();
        
        //for navigation activation
        $dataNav = array(
            "level1" => "dashboard",
            "level2" => null
        );
        return view('dashboard/index')
            ->with("dataNav", $dataNav)
            ->with("dataAktivasiUser", $dataAktivasiUser)
            ->with("dataCountSummary", $dataCountSummary)
            ->with("dataGraphNilai", $dataGraphNilai)
            ->with("dataGraphKelainan", $dataGraphKelainan)
            ->with("dataGraphNilaiTKJ", $dataGraphNilaiTKJ);
    }

    public function calculateKelainan(){
        //truncate
        Kelainan::truncate();

        $dataRikkes = Rikkes::all();
        foreach($dataRikkes as $key => $rikkes){
            $dataKelainan = array();
            $explodeKelainan = explode('; ', $rikkes->kelainan);

            foreach($explodeKelainan as $kelainan){
                if(!empty($kelainan)){
                    $dataKelainan[] = array(
                        'id_rikkes' => $rikkes->id,
                        'deskripsi' => $kelainan,
                        'kategori' => $this->checkKelainan($kelainan)
                    );
                }
            }
            
            Kelainan::insert($dataKelainan);
        }

        return redirect('/');
    }

    private function checkKelainan($kelainan){
        $kategoriKelainan = array(
            "Sedimen eritrosit pria",
            "Kolesterol",
            "leukosit Neutrofil",
            "Asam urat Pria",
            "PLT",
            "HIV",
            "Kreatinin pria",
            "BB",
            "OW",
            "Darah Leukosit",
            "Sedimen leukosit pria",
            "Sinus bradikardi",
            "TD",
            "Trigliserida",
            "SGOT",
            "Haemoglobin",
            "ND",
            "Visus VOD",
            "Gula darah puasa",
            "Laju Endap Darah",
            "Skoliosis Sedang",
            "Skoliosis ringan",
            "Sedimen eritrosit wanita",
            "Bronkopneumonia",
            "Bronkiektasis",
            "SGPT",
            "kelainan  skeleton lainnya",
            "ventrikular",
            "Trombosit",
            "Sinus aritmia",
            "CTR",
            "Pleura dan diafragma",
            "jantung",
            "Imuno Serologi VDRL",
            "Infark lama dengan ejection fraction",
            "Ureum",
            "Sinus takikardi",
            "urine",
            "Sedimen leukosit wanita",
            "Dekstrokardia situs solitus",
            "Kreatinin wanita",
            "Iskemia miokardial depresi",
            "Asam urat Wanita",
            "Imuno Serologi HBs Ag",
            "Iskemia miokardial gelombang T inversi dalam",
            "Setiap kelainan aktif pada pleura (efusi /schwarte) dan diafragma  (eventrasio, letak tinggi > 1,5 corpus vertebra)",
            "Kistik fibrosis",
            "Bilirubin",
            "TBC paru",
            "paru",
            "Penebalan pleura",
            "Mediastinum",
            "Dilatasi aorta",
            "Kelainan di pleura bekas efusi pleura yang sudah sembuh dan faal paru normal",
            "Efusi pleura",
            "Penebalan pleura dengan faal paru normal",
            "Setiap  kelainan  patologis  skeleton lainnya",
            "Mekanisme atrial kontraksi prematur atrial",
            "leukosit Monosit",
            "Iskemia miokardial perubahan segmen ST dan gelombang T yang non spesifik",
            "Atherosklerosis",
            "Pneumotoraks"
        );

        foreach($kategoriKelainan as $kategori){
            if(str_contains(strtolower($kelainan), strtolower($kategori)))
                return $kategori;
        }

        return null;
    }

    public function kirimemail(){
        $subject = "Ini subject baru";
        $view = "testmail";
        $data['nama'] = "Tommy";
        $data['website'] = "terataikesjas.com";
        
        Mail::to("tommyputrapg@gmail.com")->send(new TerataikesjasEmail($subject, $view, $data));
        
		return "Email telah dikirim";
    }
}