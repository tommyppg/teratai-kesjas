<?php
 
namespace App\Http\Controllers;
 
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\News;
use DataTables;
use App\Mail\TerataikesjasEmail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
 
class NewsController extends Controller
{
    public function lihat()
    {  

        //for navigation activation
        $dataNav = array(
            "level1" => "news",
            "level2" => "news.lihat"
        );
        return view('news/lihat')
            ->with("dataNav", $dataNav);
    }

    public function getDataNews(Request $request){
        if ($request->ajax()) {
            $data = DB::table('news')
                ->select('news.*', DB::raw('count( news_readers.id ) AS jumlah_readers '))
                ->leftJoin('news_readers', 'news_readers.id_news', '=', 'news.id')
                ->groupBy('news.id')
                ->orderBy('news.created_at', 'desc')
                ->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->editColumn('created_at', function($row){
                    return date('d-m-Y H:i:s');
                })
                ->editColumn('status', function($row){
                    if($row->status == "published"){
                        return "<span class='badge bg-success text-white'><i class='fas fa-check-circle'></i> Terpublikasikan</span>";
                    }else{
                        return "<span class='badge bg-dark text-white'>Belum publish</span>";
                    }
                })
                ->addColumn('action', function($row){
                    $actionBtn = '<button type="button" class="edit btn btn-primary btn-sm btn-update-news" data-id-news="'.$row->id.'"><i class="fas fa-edit"></i> Update</button> <button type="button" class="edit btn btn-danger btn-sm btn-delete-news" data-id-news="'.$row->id.'"><i class="fas fa-trash"></i> Hapus</button>';
                    return $actionBtn;
                })
                ->rawColumns(['action', 'status'])
                ->make(true);
        }
    }

    public function addOrUpdateNews(Request $request){
        //validate form
        $validator = Validator::make($request->all(), [
            'judul' => 'required'
        ]);

        //validator check
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()
            ], 200);
        }
        
        $news = Array();
        $news['judul'] = $request->judul;
        $news['isi'] = $request->isi;
        $news['status'] = $request->status;
        $news['created_by'] = Auth::user()->id;

        if($request->has('lampiran')){
            $path_lampiran = $request->file('lampiran')->store('public/files/news');
            
            $news['lampiran'] = pathinfo($path_lampiran)['basename'];
        }
        
        if($request->form_mode == "add"){
            $insertOrUpdate = News::create($news);
        }else{
            $insertOrUpdate = News::where('id', $request->id_news)->update($news);
        }

        if($insertOrUpdate){
            return response()->json([
                'success' => true,
                'message' => 'Data news berhasil disimpan'
            ], 200);
        }else{
            return response()->json([
                'success' => false,
                'message' => 'Gagal menyimpan, silakan coba lagi'
            ], 200);
        }
    }

    public function getNews(Request $request){
        $news = News::where('id', $request->id_news)->first();

        $news['lampiran_url'] = url('news_file/'.$news->lampiran);

        return response()->json([
            'success' => true,
            'message' => 'Success',
            'data' => $news
        ], 200);
    }

    public function deleteNews(Request $request){
        $delete = News::where('id', $request->id_news)->delete();
        
        return response()->json([
            'success' => true,
            'message' => 'Tindakan berhasil dihapus!'
        ], 200);
    }
}