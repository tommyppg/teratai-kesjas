<?php
 
namespace App\Http\Controllers;
 
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\Satker;
use App\Models\JenisPoli;
use DataTables;
use App\Mail\TerataikesjasEmail;
use Illuminate\Support\Facades\Mail;
Use Exception; 
 
class FaskesController extends Controller
{
    public function lihat()
    {  

        //for navigation activation
        $dataNav = array(
            "level1" => "faskes",
            "level2" => "faskes.lihat"
        );
        return view('faskes/lihat')
            ->with("dataNav", $dataNav);
    }

    public function getDataFaskes(Request $request)
    {  
        if ($request->ajax()) {
            $data = Satker::whereIn('tipe', ['fktp', 'rs'])
                ->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->editColumn('tipe', function($row){
                    switch($row->tipe){
                        case 'fktp':
                            return '<span class="badge bg-success text-white">FKTP</span>';
                        case 'rs':
                            return '<span class="badge bg-info text-white">Rumah Sakit</span>';
                    }
                })
                ->addColumn('action', function($row){
                    $actionBtn = '<a class="edit btn btn-success btn-sm" href="'.url('faskes/detail/'.$row->id).'"><i class="fas fa-users"></i> Lihat Akun</a> <button type="button" class="edit btn btn-primary btn-sm btn-update-faskes" data-id-faskes="'.$row->id.'"><i class="fas fa-edit"></i> Update</button> <button type="button" class="edit btn btn-danger btn-sm btn-delete-faskes" data-id-faskes="'.$row->id.'"><i class="fas fa-trash"></i> Hapus</button>';
                    return $actionBtn;
                })
                ->rawColumns(['action', 'tipe'])
                ->make(true);
        }
    }

    public function getDataFaskesPoli($id_satker, Request $request)
    {  
        if ($request->ajax()) {
            $data = Satker::select('satkers.*', 'jenis_polis.nama_jenis_poli')
                ->where(['tipe' => 'rs_poli', 'id_dibawah_koordinasi_satker' => $id_satker])
                ->join('jenis_polis', 'jenis_polis.id', '=', 'satkers.id_jenis_poli')
                ->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<a class="edit btn btn-success btn-sm" href="'.url('faskes/detail-poli/'.$row->id).'"><i class="fas fa-users"></i> Lihat Akun</a> <button type="button" class="edit btn btn-primary btn-sm btn-update-faskes" data-id-faskes="'.$row->id.'"><i class="fas fa-edit"></i> Update</button> <button type="button" class="edit btn btn-danger btn-sm btn-delete-faskes" data-id-faskes="'.$row->id.'"><i class="fas fa-trash"></i> Hapus</button>';
                    return $actionBtn;
                })
                ->rawColumns(['action', 'tipe'])
                ->make(true);
        }
    }

    public function addOrUpdateFaskes(Request $request){
        //validate form
        $validator = Validator::make($request->all(), [
            'nama' => 'required'
        ]);

        //validator check
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()
            ], 200);
        }
        
        $satker = Array();
        $satker['nama_satker'] = $request->nama;
        $satker['tipe'] = $request->tipe;

        if($request->has('id_jenis_poli')){
            $satker['id_jenis_poli'] = $request->id_jenis_poli;
        }
        
        if($request->form_mode == "add"){
            $insertOrUpdate = Satker::create($satker);
        }else{
            $insertOrUpdate = Satker::where('id', $request->id_faskes)->update($satker);
        }

        if($insertOrUpdate){
            return response()->json([
                'success' => true,
                'message' => 'Faskes berhasil disimpan'
            ], 200);
        }else{
            return response()->json([
                'success' => false,
                'message' => 'Gagal menyimpan, silakan coba lagi'
            ], 200);
        }
    }

    public function getFaskes(Request $request){
        $satker = Satker::where('id', $request->id_faskes)->first();

        return response()->json([
            'success' => true,
            'message' => 'Success',
            'data' => $satker
        ], 200);
    }

    public function deleteFaskes(Request $request){
        $delete = Satker::where('id', $request->id_faskes)->delete();
        
        return response()->json([
            'success' => true,
            'message' => 'Faskes berhasil dihapus!'
        ], 200);
    }

    public function detail($id)
    {  
        $faskes = DB::table('satkers')->where('id', $id)->first();

        //for navigation activation
        $dataNav = array(
            "level1" => "faskes",
            "level2" => "faskes.lihat"
        );

        if($faskes->tipe == "rs"){
            $jenisPoli = DB::table('jenis_polis')->get();
        }else{
            $jenisPoli = null;
        }

        return view('faskes/detail')
            ->with("dataNav", $dataNav)
            ->with("jenisPoli", $jenisPoli)
            ->with("dataFaskes", $faskes);
    }

    public function detailPoli($id)
    {  
        $faskes = DB::table('satkers')->where('id', $id)->first();

        //for navigation activation
        $dataNav = array(
            "level1" => "faskes",
            "level2" => "faskes.lihat"
        );

        return view('faskes/detail-poli')
            ->with("dataNav", $dataNav)
            ->with("dataFaskes", $faskes);
    }

    public function getDataUser($id, Request $request)
    {  
        if ($request->ajax()) {
            $data = User::where('id_satker', $id)
                ->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<button type="button" class="edit btn btn-primary btn-sm btn-update-user" data-id-user="'.$row->id.'"><i class="fas fa-edit"></i> Update</button> <button type="button" class="edit btn btn-danger btn-sm btn-delete-user" data-id-user="'.$row->id.'"><i class="fas fa-trash"></i> Hapus</button>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    public function addOrUpdateUser(Request $request){
        if($request->form_mode == "add"){
             //validate form
            $validator = Validator::make($request->all(), [
                'nama' => 'required',
                'email' => 'required|email',
                'password' => 'required',
            ]);
        }else{
             //validate form
            $validator = Validator::make($request->all(), [
                'nama' => 'required',
                'email' => 'required|email'
            ]);
        }

        //validator check
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()
            ], 200);
        }
        
        $user = Array();
        $user['nama'] = $request->nama;
        $user['email'] = $request->email;
        $user['role'] = $request->role;
        $user['id_satker'] = $request->id_satker;
        
        try {
            if($request->form_mode == "add"){
                $user['password'] = bcrypt($request->password);
    
                $insertOrUpdate = User::create($user);
                $this->sendEmailtoNewUser($user, $request->password);
            }else{
                if($request->has('ubah_password')){
                    $user['password'] = bcrypt($request->password);
                }
                $insertOrUpdate = User::where('id', $request->id_user)->update($user);
            }
    
            if($insertOrUpdate){
                return response()->json([
                    'success' => true,
                    'message' => 'Akun berhasil disimpan'
                ], 200);
            }else{
                return response()->json([
                    'success' => false,
                    'message' => 'Gagal menyimpan, silakan coba lagi'
                ], 200);
            }
        } catch (Exception  $e) {
            if($e->getCode() == "23000"){
                return response()->json([
                    'success' => false,
                    'message' => 'E-mail sudah terdaftar, silakan coba email lain'
                ], 200);
            }else{
                return response()->json([
                    'success' => false,
                    'message' => 'Gagal menyimpan, silakan coba lagi'
                ], 200);
            }
     
            return false;
        }
    }

    public function getUser(Request $request){
        $user = User::where('id', $request->id_user)->first();

        return response()->json([
            'success' => true,
            'message' => 'Success',
            'data' => $user
        ], 200);
    }

    public function deleteUser(Request $request){
        $delete = User::where('id', $request->id_user)->delete();
        
        return response()->json([
            'success' => true,
            'message' => 'Akun berhasil dihapus!'
        ], 200);
    }

    public function email(){
        return view('mail/newfaskes');
    }

    public function sendEmailtoNewUser($user, $password){
        $getUser = User::where('email', $user['email'])->join('satkers', 'satkers.id', '=', 'users.id_satker')->first();

        $subject = "Pendaftaran Akun Faskes Baru";
        $view = "newfaskes";
        $data['user'] = $getUser;
        $data['password'] = $password;
        
        Mail::to($user['email'])->send(new TerataikesjasEmail($subject, $view, $data));
    }

    public function dataMasterPoli()
    {  

        //for navigation activation
        $dataNav = array(
            "level1" => "faskes",
            "level2" => "faskes.data-master-poli"
        );
        return view('faskes/data-master-poli')
            ->with("dataNav", $dataNav);
    }

    public function getDataMasterJenisPoli(Request $request)
    {  
        if ($request->ajax()) {
            $data = JenisPoli::all();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<button type="button" class="edit btn btn-primary btn-sm btn-update-jenis-poli" data-id-jenis-poli="'.$row->id.'"><i class="fas fa-edit"></i> Update</button> <button type="button" class="edit btn btn-danger btn-sm btn-delete-jenis-poli" data-id-jenis-poli="'.$row->id.'"><i class="fas fa-trash"></i> Hapus</button>';
                    return $actionBtn;
                })
                ->rawColumns(['action', 'tipe'])
                ->make(true);
        }
    }

    public function addOrUpdateJenisPoli(Request $request){
        //validate form
        $validator = Validator::make($request->all(), [
            'nama_jenis_poli' => 'required'
        ]);

        //validator check
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()
            ], 200);
        }
        
        $jenisPoli = Array();
        $jenisPoli['nama_jenis_poli'] = $request->nama_jenis_poli;
        
        if($request->form_mode == "add"){
            $insertOrUpdate = JenisPoli::create($jenisPoli);
        }else{
            $insertOrUpdate = JenisPoli::where('id', $request->id_jenis_poli)->update($jenisPoli);
        }

        if($insertOrUpdate){
            return response()->json([
                'success' => true,
                'message' => 'Data master poli berhasil disimpan'
            ], 200);
        }else{
            return response()->json([
                'success' => false,
                'message' => 'Gagal menyimpan, silakan coba lagi'
            ], 200);
        }
    }

    public function getJenisPoli(Request $request){
        $jenisPoli = JenisPoli::where('id', $request->id_jenis_poli)->first();

        return response()->json([
            'success' => true,
            'message' => 'Success',
            'data' => $jenisPoli
        ], 200);
    }

    public function deleteJenisPoli(Request $request){
        $delete = JenisPoli::where('id', $request->id_jenis_poli)->delete();
        
        return response()->json([
            'success' => true,
            'message' => 'Data master poli berhasil dihapus!'
        ], 200);
    }
}