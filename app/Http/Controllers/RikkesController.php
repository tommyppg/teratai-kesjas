<?php
 
namespace App\Http\Controllers;
 
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\Rikkes;
use Illuminate\Support\Facades\Hash;
use DataTables;
use App\Models\Kelainan;
use App\Models\DaftarKelainan;
use App\Models\PoliDituju;
 
class RikkesController extends Controller
{
    /**
     * Show the profile for a given user.
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function index()
    {  

        //for navigation activation
        $dataNav = array(
            "level1" => "rikkes",
            "level2" => "rikkes.import"
        );
        return view('rikkes/import')
            ->with("dataNav", $dataNav);
    }

    public function import(Request $request){
        //validate form
        $validator = Validator::make($request->all(), [
            'excel' => 'required|mimes:xls,xlsx',
            'tahun_rikkes' => 'required|numeric'
        ]);

        //validator check
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()
            ], 200);
        }

        //get imported file from request and tear into array
        $importExcel = Excel::toArray([], $request->file('excel'));
        $daftarKelainan = DaftarKelainan::all();

        //lets foreach
        foreach($importExcel[0] as $key => $dataExcel){
            //temp var
            $dataAnggota = null;
            $dataRikkes = null;

            if($dataExcel[0] != null){
                if($dataExcel[0] != "NO" && $dataExcel[1] != "NAMA"){
                    //explode pangkat NRP
                    $explodePangkatNRP = explode(" / ", $dataExcel[3]);
                    
                    //add data to temp anggota
                    $dataAnggota = array(
                        'nama' => $dataExcel[1],
                        'role' => "anggota",
                        'nrp' => $explodePangkatNRP[1],
                        'pangkat' => $explodePangkatNRP[0],
                        'satker' => $dataExcel[7],
                        'jenis_kelamin' => $dataExcel[4],
                        'password' => Hash::make($explodePangkatNRP[1], [
                            'rounds' => 5,
                        ]),
                        // 'profile_status' => 'uncomplete',
                        // 'password_status' => 'uncomplete',
                        'status' => 'active'
                    );

                    //add data to temp rikkes
                    $dataRikkes = array(
                        'nrp' => $explodePangkatNRP[1],
                        'tahun_rikkes' => $request->tahun_rikkes,
                        'intensif' => $dataExcel[5],
                        'tb' => (float) $dataExcel[8],
                        'bb' => (float) $dataExcel[9],
                        'sist' => $dataExcel[10],
                        'dias' => $dataExcel[11],
                        'nadi' => $dataExcel[12],
                        'kelainan' => $dataExcel[14],
                        'kualitas' => $dataExcel[20],
                        'kuantitas' => $dataExcel[22],
                        'status' => 'locked'
                    );

                    //insert to db user
                    User::upsert($dataAnggota, ['nrp']);
                    $idRikkes = Rikkes::insertGetId($dataRikkes);
                    
                    $dataKelainan = array();
                    // $dataPoliDituju = array();
                    $explodeKelainan = explode('; ', $dataRikkes['kelainan']);

                    foreach($explodeKelainan as $kelainan){
                        if(!empty($kelainan)){
                            $checkKelainan = $this->checkKelainan($daftarKelainan, $kelainan);
                            $dataKelainan[] = array(
                                'id_rikkes' => $idRikkes,
                                'deskripsi' => $kelainan,
                                'id_daftar_kelainan' => $checkKelainan->id
                            );

                            // if(!in_array($checkKelainan->id_kategori_penyakit, $dataPoliDituju)){
                            //     $dataPoliDituju[] = $checkKelainan->id_kategori_penyakit;
                            // }

                        }
                    }

                    // //poli dituju
                    // $insertPoliDituju = array();
                    // foreach($dataPoliDituju as $poliDituju){
                    //     $insertPoliDituju[] = array(
                    //         'id_rikkes' => $idRikkes,
                    //         'id_kategori_penyakit' => $poliDituju,
                    //         'status' => 'undone'
                    //     );
                    // }
                    
                    // PoliDituju::insert($insertPoliDituju);
                    Kelainan::insert($dataKelainan);
                }
            }
        }
        return response()->json([
            'success' => true,
            'message' => "Import berhasil"
        ], 200);
    }

    private function checkKelainan($daftarKelainan, $kelainan){
        foreach($daftarKelainan as $dKelainan){
            if(str_contains($kelainan, $dKelainan->nama_kelainan))
                return $dKelainan;
        }
        return null;
    }

    public function lihat()
    {  

        //for navigation activation
        $dataNav = array(
            "level1" => "rikkes",
            "level2" => "rikkes.lihat"
        );
        return view('rikkes/lihat')
            ->with("dataNav", $dataNav);
    }

    public function getData(Request $request)
    {  
        if ($request->ajax()) {
            // $data = Rikkes::join('users', 'rikkes.nrp', '=', 'users.nrp')
            //     ->latest('rikkes.created_at')
            //     ->get();
            $data = Rikkes::select('rikkes.*', 'users.nama', 'users.nrp')
                ->join('users', 'rikkes.nrp', '=', 'users.nrp')
                ->latest('rikkes.created_at')
                ->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<a href="'.url('anggota/detail/'.$row->nrp).'" class="edit btn btn-primary btn-sm"><i class="fas fa-eye"></i> Detail</a>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    public function sudahPeriksa()
    {  

        //for navigation activation
        $dataNav = array(
            "level1" => "rikkes",
            "level2" => "rikkes.sudah-periksa"
        );
        return view('rikkes/sudah-periksa')
            ->with("dataNav", $dataNav);
    }

    public function getDataSudahPeriksa(Request $request)
    {  
        if ($request->ajax()) {
            // $data = Rikkes::join('users', 'rikkes.nrp', '=', 'users.nrp')
            //     ->latest('rikkes.created_at')
            //     ->get();
            $data = Rikkes::select('rikkes.*', 'users.nama', 'users.nrp')
                ->join('users', 'rikkes.nrp', '=', 'users.nrp')
                ->where('rikkes.status', 'unlocked')
                ->latest('rikkes.created_at')
                ->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<a href="'.url('anggota/detail/'.$row->nrp).'" class="edit btn btn-primary btn-sm"><i class="fas fa-eye"></i> Detail</a>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    public function detail($nrp)
    {  
        //for navigation activation
        $dataNav = array(
            "level1" => "rikkes",
            "level2" => "rikkes.lihat"
        );

        $anggota = DB::table('users')->where('nrp', $nrp)->first();
        // dd($anggota);
        return view('rikkes/detail')
            ->with("dataNav", $dataNav)
            ->with("dataAnggota", $anggota);
    }
}