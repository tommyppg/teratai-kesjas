<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Session;

class AuthController extends Controller
{
    //index
    public function index(){
        if (Auth::check()) {
            return redirect('/');
        }
        return view('auth/login');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function authenticate(Request $request)
    {
        $email      = $request->input('email');
        $password   = $request->input('password');
        
        $users = DB::table('users')
                ->where('email', $email)
                ->first();

        if($users){
            if($users->role == "superadmin" || $users->role == "fktp" || $users->role == "rs" || $users->role == "rs_poli"){
                if(Auth::attempt(['email' => $email, 'password' => $password])) {
                    if($users->role == "faskes"){
                        $satker = DB::table('satkers')
                                ->where('id', Auth::user()->id_satker)
                                ->first();
                        Session::put('satker', $satker->nama_satker);
                    }
                    
                    return response()->json([
                        'success' => true
                    ], 200);
                } else {
                    return response()->json([
                        'success' => false,
                        'message' => 'Username atau password tidak ditemukan.'
                    ], 200);
                }
            }else{
                return response()->json([
                    'success' => false,
                    'message' => 'Username atau password tidak ditemukan.'
                ], 200);
            }
        }else{
            return response()->json([
                'success' => false,
                'message' => 'Username atau password tidak ditemukan.'
            ], 200);
        }

        
    }

    public function logout(){
        Auth::logout();
        return redirect('/login');
    }

    //index
    public function setNewPassword($token){
        return view('auth/set-new-password')->with('token', $token);
    }

    public function setNewPasswordProccess(Request $request){
        $validator = Validator::make($request->all(), [
            'password' => 'required',
            'confirm_password' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => 'Mohon pastikan formulir terisi dengan sempurna'
            ], 200);
        }
        
        $checkToken = DB::table('password_resets')->where('token', $request->token)->first();

        if(!$checkToken){
            return response()->json([
                'success' => false,
                'message' => 'Invalid token!'
            ], 200);
        }

        //set password
        DB::table('users')->where('email', $checkToken->email)->update(['password' => bcrypt($request->password)]);

        //delete reset password token
        DB::table('password_resets')->where(['email'=> $checkToken->email])->delete();

        return response()->json([
            'success' => true
        ], 200);
    }
}
