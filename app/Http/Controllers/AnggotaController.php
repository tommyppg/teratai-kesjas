<?php
 
namespace App\Http\Controllers;
 
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\Rikkes;
use DataTables;
 
class AnggotaController extends Controller
{
    /**
     * Show the profile for a given user.
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function index()
    {  

        //for navigation activation
        $dataNav = array(
            "level1" => "anggota",
            "level2" => "anggota.index"
        );
        return view('anggota/index')
            ->with("dataNav", $dataNav);
    }

    public function lihat()
    {  

        //for navigation activation
        $dataNav = array(
            "level1" => "anggota",
            "level2" => "anggota.lihat"
        );
        return view('anggota/lihat')
            ->with("dataNav", $dataNav);
    }

    public function getData(Request $request)
    {  
        if ($request->ajax()) {
            
            $data = User::select('users.*', 'rikkes.status as rikkes_status', 'tkj.nilai_akhir as tkj_status')
                ->leftJoin('rikkes', 'rikkes.nrp', '=', 'users.nrp')
                ->leftJoin('tkj', 'tkj.nrp', '=', 'users.nrp')
                ->where('role', 'anggota')
                ->where(function($query) use($request){
                    $query->where('rikkes.tahun_rikkes', $request->tahun)
                    ->orWhere('tkj.tahun_tkj', $request->tahun);
                })
                ->when($request->status_rikkes != "", function($query) use($request){
                    if($request->status_rikkes == "sudah_periksa"){
                        $where_status_rikkes = DB::raw('rikkes.status = "unlocked"');
                    }else if($request->status_rikkes == "belum_periksa"){
                        $where_status_rikkes = DB::raw('rikkes.status = "locked"');
                    }else if($request->status_rikkes == "belum_rikkes"){
                        $where_status_rikkes = DB::raw('rikkes.status IS NULL');
                    }
                    $query->whereRaw($where_status_rikkes);
                })
                ->when($request->status_tkj != "", function($query) use($request){
                    if($request->status_tkj == "sudah_tkj"){
                        $where_status_tkj = DB::raw('tkj.nilai_akhir IS NOT NULL');
                    }else if($request->status_tkj == "belum_tkj"){
                        $where_status_tkj = DB::raw('tkj.nilai_akhir IS NULL');
                    }
                    $query->whereRaw($where_status_tkj);
                });
                
            return DataTables::of($data)
                ->skipTotalRecords()
                ->addIndexColumn()
                ->editColumn('rikkes_status', function($row){
                    switch($row->rikkes_status){
                        case 'unlocked':
                            return '<span class="badge bg-success text-white">Sudah periksa</span>';
                            break;
                        case 'locked':
                            return '<span class="badge bg-danger text-white">Belum Periksa</span>';
                            break;
                        default:
                            return '<span class="badge bg-dark text-white">Belum Rikkes</span>';
                            break;
                    }
                })
                ->editColumn('tkj_status', function($row){
                    if($row->tkj_status == null){
                        return '<span class="badge bg-dark text-white">Belum TKJ</span>';
                    }else{
                        return '<span class="badge bg-success text-white">Sudah TKJ</span>';
                    }
                })
                ->editColumn('status', function($row){
                    switch($row->status){
                        case 'active':
                            return '<span class="badge bg-success text-white">Aktif</span>';
                            break;
                        case 'inactive':
                            return '<span class="badge bg-dark text-white">Non aktif</span>';
                            break;
                        default:
                            break;
                    }
                })
                ->addColumn('action', function($row){
                    $actionBtn = '<a href="'.url('anggota/detail/'.$row->nrp).'" class="edit btn btn-success btn-sm"><i class="fas fa-eye"></i> Lihat</a>';

                    if($row->status == "active"){
                        $actionBtn .= ' <button type="button" class="btn btn-danger btn-sm btn-status-change" data-nama-user="'.$row->nama.'" data-current-status="active" data-nrp="'.$row->nrp.'"><i class="fas fa-ban"></i> Nonaktifkan</button>';
                    }else{
                        $actionBtn .= ' <button type="button" class="btn btn-info btn-sm btn-status-change" data-nama-user="'.$row->nama.'" data-current-status="inactive" data-nrp="'.$row->nrp.'"><i class="fas fa-check"></i> Aktifkan</button>';
                    }
                    return $actionBtn;
                })
                ->rawColumns(['action', 'status','rikkes_status','tkj_status'])
                ->make(true);
        }
    }

    public function detail($nrp)
    {  
        //for navigation activation
        $dataNav = array(
            "level1" => "anggota",
            "level2" => "anggota.lihat"
        );

        $anggota = DB::table('users')->where('nrp', $nrp)->first();
        // dd($anggota);
        return view('anggota/detail')
            ->with("dataNav", $dataNav)
            ->with("dataAnggota", $anggota);
    }

    public function changeStatusUser(Request $request){
        $nrp = $request->nrp;
        $current_status = $request->current_status;

        $user['status'] = $current_status == "active" ? "inactive" : "active";

        $update = DB::table('users')->where('nrp', $nrp)->update($user);

        if($update) {
            return response()->json([
                'success' => true,
                'message' => "Berhasil update status anggota"
            ], 200);
        }else{
            return response()->json([
                'success' => false,
                'message' => "Gagal update status anggota"
            ], 200);
        }
    }
}