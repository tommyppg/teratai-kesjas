<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Session;

class PrivacyPolicyController extends Controller
{
    public function privacyPolicy(){
        return view('privacy-policy/show');
    }
}