<?php
 
namespace App\Http\Controllers;
 
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\Rikkes;
use App\Models\TKJ;
use App\Models\Tindakan;
use App\Models\PoliDituju;
use App\Models\JenisPoli;
use DataTables;
use Config;
 
class TindakanController extends Controller
{
    public function lihat()
    {  

        //for navigation activation
        $dataNav = array(
            "level1" => "tindakan",
            "level2" => "tindakan.lihat"
        );
        return view('tindakan/lihat')
            ->with("dataNav", $dataNav);
    }

    public function getDataAnggota(Request $request)
    {  
        if ($request->ajax()) {
            $data = User::latest()
                ->where('role', 'anggota')
                ->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<a href="'.url('tindakan/detail/'.$row->nrp).'" class="edit btn btn-primary btn-sm"><i class="fas fa-eye"></i> Detail</a>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    public function detail($nrp)
    {  
        //for navigation activation
        $dataNav = array(
            "level1" => "tindakan",
            "level2" => "tindakan.lihat"
        );

        $anggota = DB::table('users')->where('nrp', $nrp)->first();
        // dd($anggota);
        return view('tindakan/detail')
            ->with("dataNav", $dataNav)
            ->with("dataAnggota", $anggota);
    }

    public function detailTindakan($id_rikkes)
    {  
        //for navigation activation
        $dataNav = array(
            "level1" => "tindakan",
            "level2" => "tindakan.lihat"
        );

        $rikkes = DB::table('rikkes')->where('id', $id_rikkes)->first();
        $anggota = DB::table('users')->where('nrp', $rikkes->nrp)->first();
        $jenisPoli = DB::table('jenis_polis')->get();
        // dd($anggota);
        return view('tindakan/detail-tindakan')
            ->with("dataNav", $dataNav)
            ->with("dataRikkes", $rikkes)
            ->with("dataAnggota", $anggota)
            ->with("jenisPoli", $jenisPoli);
    }

    public function getDataRikkes($nrp, Request $request)
    {  
        if ($request->ajax()) {
            $data = Rikkes::select('rikkes.*')
                ->join('users', 'rikkes.nrp', '=', 'users.nrp')
                ->where('rikkes.nrp', $nrp)
                ->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('info', function($row){
                    $info = "TB: ".$row->tb."<br/>";
                    $info .= "BB: ".$row->bb."<br/>";
                    $info .= "Sist: ".$row->sist."<br/>";
                    $info .= "Dias: ".$row->dias."<br/>";
                    $info .= "Nadi: ".$row->nadi;
                    return $info;
                })
                ->addColumn('nilai', function($row){
                    switch($row->kualitas){
                        case 'Baik (B)':
                            return '<span class="badge bg-success text-white">Baik (B)</span>';
                            break;
                        case 'Cukup (C)':
                            return '<span class="badge bg-info text-white">Cukup (C)</span>';
                            break;
                        case 'Kurang (K1)':
                            return '<span class="badge bg-warning text-white">Kurang (K1)</span>';
                            break;
                        case 'Kurang Sekali (K2)':
                            return '<span class="badge bg-dark text-white">Kurang Sekali (K2)</span>';
                            break;
                        default:
                            break;
                    }
                })
                ->addColumn('poli_dituju', function($row){
                    $dataPoliDituju = PoliDituju::select('jenis_polis.id as id_jenis_poli', 'nama_jenis_poli')
                    ->where('poli_ditujus.id_rikkes', $row->id)
                    ->join('jenis_polis', 'jenis_polis.id', '=', 'poli_ditujus.id_jenis_poli')
                    ->orderBy('id_jenis_poli')
                    ->get();

                    if(count($dataPoliDituju) > 0){
                        $poliDitujuPrint = "<ol>";
                        foreach($dataPoliDituju as $key => $poliDituju){
                            $poliDitujuPrint .= "<li>".$poliDituju->nama_jenis_poli. "</li>";
                        }
                        $poliDitujuPrint .= "</ol>";

                        return $poliDitujuPrint;
                    }else{
                        return "-";
                    }
                })
                ->addColumn('action', function($row){
                    $actionBtn = '<a href="'.url('tindakan/detail-tindakan/'.$row->id).'" class="edit btn btn-primary btn-sm"><i class="fas fa-laptop-medical"></i> Tindakan</a>';
                    return $actionBtn;
                })
                ->rawColumns(['action', 'nilai', 'info', 'poli_dituju'])
                ->make(true);
        }
    }

    public function getDataTKJ($nrp, Request $request)
    {  
        if ($request->ajax()) {
            $data = TKJ::select('tkj.*')
                ->join('users', 'tkj.nrp', '=', 'users.nrp')
                ->where('tkj.nrp', $nrp)
                ->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('nilai', function($row){
                    switch($row->kategori){
                        case 'BS':
                            return '<span class="badge bg-primary text-white">Baik Sekali (BS)</span>';
                            break;
                        case 'B':
                            return '<span class="badge bg-success text-white">Baik (B)</span>';
                            break;
                        case 'C':
                            return '<span class="badge bg-info text-white">Cukup (C)</span>';
                            break;
                        case 'K':
                            return '<span class="badge bg-warning text-white">Kurang (K)</span>';
                            break;
                        case 'KS':
                            return '<span class="badge bg-dark text-white">Kurang Sekali (KS)</span>';
                            break;
                        default:
                            break;
                    }
                })
                ->addColumn('action', function($row){
                    $actionBtn = '<a href="'.url('tindakan/detail-tindakan/'.$row->id).'" class="edit btn btn-primary btn-sm"><i class="fas fa-laptop-medical"></i> Tindakan</a>';
                    return $actionBtn;
                })
                ->rawColumns(['action', 'nilai'])
                ->make(true);
        }
    }

    public function getDataTindakan($id_rikkes, Request $request)
    {  
        if ($request->ajax()) {
            $data = Tindakan::select('tindakans.*','satkers.nama_satker')
                ->leftjoin('satkers', 'tindakans.id_satker_faskes', '=', 'satkers.id')
                ->where('tindakans.id_rikkes', $id_rikkes)
                ->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->editColumn('status', function($row){
                    switch($row->status){
                        case 'verified':
                            return '<span class="badge bg-success text-white"><i class="fas fa-check-circle"></i> Terverifikasi</span>';
                        case 'unverified':
                            return '<span class="badge bg-danger text-white">Belum terverifikasi</span>';
                    }
                })
                ->editColumn('jenis', function($row){
                    switch($row->jenis){
                        case 'tindakan_akhir':
                            return "Tindakan Akhir";
                        case 'butuh_rujukan':
                            return "Butuh Rujukan";
                    }
                })
                ->addColumn('poli_dituju', function($row){
                    if($row->jenis == "butuh_rujukan"){
                        $dataPoliDituju = PoliDituju::join('jenis_polis', 'jenis_polis.id', '=', 'poli_ditujus.id_jenis_poli')->where('id_tindakan', $row->id)->get();

                        $poliDituju = "<ol>";
                        foreach($dataPoliDituju as $dPoliDituju){
                            $poliDituju .= "<li>".$dPoliDituju->nama_jenis_poli."</li>";
                        }
                        $poliDituju .= "</ol>";
                    }else{
                        $poliDituju = "-";
                    }
                    
                    return $poliDituju;
                })
                ->addColumn('action', function($row){
                    if($row->id_satker_faskes == Auth::user()->id_satker){
                        $actionBtn = '<button type="button" class="edit btn btn-primary btn-sm btn-edit-tindakan" data-id-tindakan="'.$row->id.'"><i class="fas fa-edit"></i> Update</button> <button type="button" class="edit btn btn-danger btn-sm btn-hapus-tindakan" data-id-tindakan="'.$row->id.'"><i class="fas fa-trash"></i> Hapus</button>';
                    }else{
                        // $actionBtn = '<button type="button" class="edit btn btn-primary btn-sm"><i class="fas fa-edit" disabled></i> Update</button>';
                        $actionBtn = null;
                    }
                    
                    return $actionBtn;
                })
                ->editColumn('created_at', function($row){
                    return date('d-m-Y H:i:s', strtotime($row->created_at));
                })
                ->editColumn('tipe', function($row){
                    if($row->tipe == "faskes"){
                        return "<span class='badge bg-info text-white'>Faskes</span>";
                    }else{
                        return "<span class='badge bg-primary text-white'>Pribadi</span>";
                    }
                })
                ->editColumn('nama_satker', function($row){
                    switch($row->tipe){
                        case 'faskes':
                            return $row->nama_satker;
                        case 'pribadi':
                            return $row->faskes_pribadi;
                        default:
                            return "N/A";
                    }
                })
                ->rawColumns(['action', 'tindakan', 'tipe', 'poli_dituju', 'status'])
                ->make(true);
        }
    }

    public function addOrUpdateTindakan(Request $request){
        //validate form
        $validator = Validator::make($request->all(), [
            'deskripsi_tindakan' => 'required',
        ]);

        //validator check
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()
            ], 200);
        }
        
        $tindakan = Array();
        $tindakan['id_rikkes'] = $request->id_rikkes;
        $tindakan['id_satker_faskes'] = Auth::user()->id_satker;
        $tindakan['tindakan'] = $request->deskripsi_tindakan;
        $tindakan['jenis'] = $request->jenis;
        $tindakan['tipe'] = 'faskes';
        $tindakan['status'] = 'verified';

        switch(Auth::user()->role){
            case 'fktp':
            case 'rs':
                if($request->form_mode == "add"){
                    $tindakan['created_at'] = date("Y-m-d H:i:s");
                    $insertOrUpdate = Tindakan::insertGetId($tindakan);
        
                    if($tindakan['jenis'] == "butuh_rujukan"){
                        $poli_dituju = $request->poli_dituju;
            
                        foreach($poli_dituju as $poli){
                            $insertPoliDituju = array(
                                'id_rikkes' => $request->id_rikkes,
                                'id_tindakan' => $insertOrUpdate,
                                'id_jenis_poli' => $poli,
                                'status' => 'undone'
                            );
                            
                            PoliDituju::create($insertPoliDituju);
                        }
                    }else if($tindakan['jenis'] == "tindakan_akhir"){
                        Rikkes::where('id', $request->id_rikkes)->update(['status' => 'unlocked']);
                    }
                }else{
                    if($tindakan['jenis'] == "butuh_rujukan"){
                        $deletePoliDituju = PoliDituju::where('id_rikkes', $request->id_rikkes)->delete();

                        $poli_dituju = $request->poli_dituju;
            
                        foreach($poli_dituju as $poli){
                            $insertPoliDituju = array(
                                'id_rikkes' => $request->id_rikkes,
                                'id_tindakan' => $request->id_tindakan,
                                'id_jenis_poli' => $poli,
                                'status' => 'undone'
                            );
                            
                            PoliDituju::create($insertPoliDituju);
                        }
                    }
                    
                    $tindakan['updated_at'] = date("Y-m-d H:i:s");
                    $insertOrUpdate = Tindakan::where('id', $request->id_tindakan)->update($tindakan);
                }
                break;
            case 'rs_poli':
                if($request->form_mode == "add"){
                    $tindakan['created_at'] = date("Y-m-d H:i:s");
                    $insertOrUpdate = Tindakan::insertGetId($tindakan);
                    
                    //ambil jenis poli dari akun poli yang sedang login
                    $getThisAccountPoli = DB::table('satkers')->join('jenis_polis', 'jenis_polis.id', '=', 'satkers.id_jenis_poli')->where('satkers.id', Auth::user()->id_satker)->first();

                    $where = array(
                        'id_rikkes' => $request->id_rikkes,
                        'id_jenis_poli' => $getThisAccountPoli->id_jenis_poli
                    );

                    $update = array(
                        'status' => 'done'
                    );
                    $updateThisPoliDituju = DB::table('poli_ditujus')->where($where)->update($update);

                    //check unlocked
                    $where = array(
                        'id_rikkes' => $request->id_rikkes,
                        'status' => 'undone'
                    );

                    $checkUndone = DB::table('poli_ditujus')->where($where)->get();

                    if(count($checkUndone) == 0){
                        Rikkes::where('id', $request->id_rikkes)->update(['status' => 'unlocked']);
                    }
                }else{
                    $tindakan['updated_at'] = date("Y-m-d H:i:s");
                    $insertOrUpdate = Tindakan::where('id', $request->id_tindakan)->update($tindakan);
                }
                break;
        }
        

        if($insertOrUpdate){
            return response()->json([
                'success' => true,
                'message' => 'Tindakan berhasil disimpan'
            ], 200);
        }else{
            return response()->json([
                'success' => false,
                'message' => 'Gagal insert, silakan coba lagi'
            ], 200);
        }
    }

    public function getTindakan(Request $request){
        $tindakan = Tindakan::where('id', $request->id_tindakan)->first();

        return response()->json([
            'success' => true,
            'message' => 'Success',
            'data' => $tindakan
        ], 200);
    }

    public function deleteTindakan(Request $request){
        $delete = Tindakan::where('id', $request->id_tindakan)->delete();
        $deletePoliDituju = PoliDituju::where('id_rikkes', $request->id_rikkes)->delete();
        
        return response()->json([
            'success' => true,
            'message' => 'Tindakan berhasil dihapus!'
        ], 200);
    }

    public function verifikasi()
    {  

        //for navigation activation
        $dataNav = array(
            "level1" => "tindakan",
            "level2" => "tindakan.verifikasi"
        );
        return view('tindakan/verifikasi')
            ->with("dataNav", $dataNav);
    }

    public function getDataVerifikasi(Request $request)
    {  
        if ($request->ajax()) {
            $data = DB::table('tindakans as t')->select('t.*','s.nama_satker', 'u.nrp', 'u.nama', 'u.satker')
                ->leftjoin('satkers as s', 't.id_satker_faskes', '=', 's.id')
                ->join('rikkes as r', 'r.id', '=', 't.id_rikkes')
                ->join('users as u', 'u.nrp', '=', 'r.nrp')
                ->where('t.status', 'unverified')
                ->get();
                
            return Datatables::of($data)
                ->addIndexColumn()
                ->editColumn('status', function($row){
                    switch($row->status){
                        case 'verified':
                            return '<span class="badge bg-success text-white"><i class="fas fa-check-circle"></i> Terverifikasi</span>';
                        case 'unverified':
                            return '<span class="badge bg-danger text-white">Belum terverifikasi</span>';
                    }
                })
                ->editColumn('jenis', function($row){
                    switch($row->jenis){
                        case 'tindakan_akhir':
                            return "Tindakan Akhir";
                        case 'butuh_rujukan':
                            return "Butuh Rujukan";
                    }
                })
                ->addColumn('poli_dituju', function($row){
                    if(!empty($row->id_jenis_poli)){
                        $dataPoli = JenisPoli::where('id', $row->id_jenis_poli)->first();

                        return $dataPoli->nama_jenis_poli;
                    }else{
                        return "-";
                    }
                })
                ->addColumn('action', function($row){
                    $actionBtn = '<button type="button" class="edit btn btn-success btn-sm btn-verifikasi" data-id-tindakan="'.$row->id.'" data-foto-resep="'.Config::get('app.api_url').'api/storage/'.$row->foto_resep.'" data-foto-obat="'.Config::get('app.api_url').'api/storage/'.$row->foto_obat.'" data-foto-kwitansi="'.Config::get('app.api_url').'api/storage/'.$row->foto_kwitansi.'" data-nomor-pendaftaran="'.$row->nomor_pendaftaran.'"><i class="fas fa-check"></i> Verifikasi</button>';
                    
                    return $actionBtn;
                })
                ->editColumn('created_at', function($row){
                    return date('d-m-Y H:i:s', strtotime($row->created_at));
                })
                ->editColumn('tipe', function($row){
                    if($row->tipe == "faskes"){
                        return "<span class='badge bg-info text-white'>Faskes</span>";
                    }else{
                        return "<span class='badge bg-primary text-white'>Pribadi</span>";
                    }
                })
                ->editColumn('nama_satker', function($row){
                    switch($row->tipe){
                        case 'faskes':
                            return $row->nama_satker;
                        case 'pribadi':
                            return $row->faskes_pribadi;
                        default:
                            return "N/A";
                    }
                })
                ->rawColumns(['action', 'tindakan', 'tipe', 'poli_dituju', 'status'])
                ->make(true);
        }
    }

    public function verifikasiProcess(Request $request){
        $tindakan = Tindakan::where('id', $request->id_tindakan)->first();

        if($request->has('id_jenis_poli') || $request->id_jenis_poli != ""){
            $where = array(
                'id_rikkes' => $tindakan->id_rikkes,
                'id_jenis_poli' => $tindakan->id_jenis_poli
            );
    
            $update = array(
                'status' => 'done'
            );
            $updateThisPoliDituju = DB::table('poli_ditujus')->where($where)->update($update);
    
            //check unlocked
            $where = array(
                'id_rikkes' => $tindakan->id_rikkes,
                'status' => 'undone'
            );
    
            $checkUndone = DB::table('poli_ditujus')->where($where)->get();
    
            if(count($checkUndone) == 0){
                Rikkes::where('id', $tindakan->id_rikkes)->update(['status' => 'unlocked']);
            }
        }

        Tindakan::where('id', $request->id_tindakan)->update(['status' => 'verified']);

        return response()->json([
            'success' => true,
            'message' => 'Berhasil memveifikasi tindakan'
        ], 200);
    }
}