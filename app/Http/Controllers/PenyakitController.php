<?php
 
namespace App\Http\Controllers;
 
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\Rikkes;
use App\Models\TKJ;
use App\Models\Tindakan;
use App\Models\KategoriPenyakit;
use App\Models\DaftarKelainan;
use DataTables;
 
class PenyakitController extends Controller
{
    public function index()
    {  
        //for navigation activation
        $dataNav = array(
            "level1" => "penyakit",
            "level2" => "penyakit.lihat"
        );
        return view('penyakit/lihat')
            ->with("dataNav", $dataNav);
    }

    public function getDataMonitoring(Request $request){
        if ($request->ajax()) {
            $data = DB::select('
            SELECT
                table_penyakit.id_kategori_penyakit,
                table_penyakit.nama_kategori_penyakit,
                COUNT(table_penyakit.nama_kategori_penyakit) as "jumlah"
            FROM
                (
                SELECT
                    r.nrp,
                    k.deskripsi,
                    dk.nama_kelainan,
                    kp.id as id_kategori_penyakit,
                    kp.nama_kategori_penyakit 
                FROM
                    rikkes AS r
                    RIGHT JOIN kelainans AS k ON k.id_rikkes = r.id
                    JOIN daftar_kelainans AS dk ON dk.id = k.id_daftar_kelainan
                    JOIN kategori_penyakits AS kp ON kp.id = dk.id_kategori_penyakit 
                GROUP BY
                    kp.nama_kategori_penyakit,
                    r.id 
                ORDER BY
                r.id 
                ) AS table_penyakit
            GROUP BY table_penyakit.nama_kategori_penyakit
            ');
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<a href="'.url('penyakit/detail/'.$row->id_kategori_penyakit).'" class="edit btn btn-primary btn-sm"><i class="fas fa-eye"></i> Detail</a>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    public function detail($id_kategori_penyakit)
    {  
        $kategoriPenyakit = KategoriPenyakit::where('id', $id_kategori_penyakit)->first();
        
        //for navigation activation
        $dataNav = array(
            "level1" => "penyakit",
            "level2" => "penyakit.lihat"
        );
        return view('penyakit/detail')
            ->with("dataNav", $dataNav)
            ->with("kategoriPenyakit", $kategoriPenyakit);
    }

    public function getDataDetailMonitoring($id_kategori_penyakit, Request $request){
        if ($request->ajax()) {
            $data = DB::select('
            SELECT
                r.*,
                u.*
            FROM
                rikkes AS r
                JOIN users AS u ON u.nrp = r.nrp
                RIGHT JOIN kelainans AS k ON k.id_rikkes = r.id
                JOIN daftar_kelainans AS dk ON dk.id = k.id_daftar_kelainan
                JOIN kategori_penyakits AS kp ON kp.id = dk.id_kategori_penyakit
            WHERE
                kp.id = '.$id_kategori_penyakit.'
            GROUP BY
                kp.nama_kategori_penyakit,
                r.id 
            ORDER BY
            r.nrp
            ');
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<a href="'.url('anggota/detail/'.$row->nrp).'" class="edit btn btn-primary btn-sm"><i class="fas fa-eye"></i> Detail</a>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    public function dataMaster()
    {  
        //for navigation activation
        $dataNav = array(
            "level1" => "penyakit",
            "level2" => "penyakit.data-master"
        );
        return view('penyakit/data-master')
            ->with("dataNav", $dataNav);
    }

    public function getDataMasterKategoriPenyakit(Request $request){
        if ($request->ajax()) {
            $data = KategoriPenyakit::all();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<a class="edit btn btn-success btn-sm" href="'.url('penyakit/data-master-daftar-kelainan/'.$row->id).'"><i class="fas fa-users"></i> Lihat Daftar Kelainan</a> <button type="button" class="edit btn btn-primary btn-sm btn-update-kategori-penyakit" data-id-kategori-penyakit="'.$row->id.'"><i class="fas fa-edit"></i> Update</button> <button type="button" class="edit btn btn-danger btn-sm btn-delete-kategori-penyakit" data-id-kategori-penyakit="'.$row->id.'"><i class="fas fa-trash"></i> Hapus</button>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    public function addOrUpdateKategoriPenyakit(Request $request){
        //validate form
        $validator = Validator::make($request->all(), [
            'nama_kategori_penyakit' => 'required'
        ]);

        //validator check
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()
            ], 200);
        }
        
        $kategoriPenyakit = Array();
        $kategoriPenyakit['nama_kategori_penyakit'] = $request->nama_kategori_penyakit;
        
        if($request->form_mode == "add"){
            $insertOrUpdate = KategoriPenyakit::create($kategoriPenyakit);
        }else{
            $insertOrUpdate = KategoriPenyakit::where('id', $request->id_kategori_penyakit)->update($kategoriPenyakit);
        }

        if($insertOrUpdate){
            return response()->json([
                'success' => true,
                'message' => 'Kategori penyakit berhasil disimpan'
            ], 200);
        }else{
            return response()->json([
                'success' => false,
                'message' => 'Gagal menyimpan, silakan coba lagi'
            ], 200);
        }
    }

    public function getKategoriPenyakit(Request $request){
        $kategoriPenyakit = KategoriPenyakit::where('id', $request->id_kategori_penyakit)->first();

        return response()->json([
            'success' => true,
            'message' => 'Success',
            'data' => $kategoriPenyakit
        ], 200);
    }

    public function deleteKategoriPenyakit(Request $request){
        $delete = KategoriPenyakit::where('id', $request->id_kategori_penyakit)->delete();
        
        return response()->json([
            'success' => true,
            'message' => 'Kategori penyakit berhasil dihapus!'
        ], 200);
    }

    public function dataMasterDaftarKelainan($id_kategori_penyakit)
    {  
        $kategoriPenyakit = KategoriPenyakit::where('id', $id_kategori_penyakit)->first();

        //for navigation activation
        $dataNav = array(
            "level1" => "penyakit",
            "level2" => "penyakit.data-master"
        );
        return view('penyakit/data-master-daftar-kelainan')
            ->with("dataNav", $dataNav)
            ->with("kategoriPenyakit", $kategoriPenyakit);
    }

    public function getDataMasterDaftarKelainan($id_kategori_penyakit, Request $request){
        if ($request->ajax()) {
            $data = DaftarKelainan::where('id_kategori_penyakit', $id_kategori_penyakit)->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<button type="button" class="edit btn btn-primary btn-sm btn-update-daftar-kelainan" data-id-daftar-kelainan="'.$row->id.'"><i class="fas fa-edit"></i> Update</button> <button type="button" class="edit btn btn-danger btn-sm btn-delete-daftar-kelainan" data-id-daftar-kelainan="'.$row->id.'"><i class="fas fa-trash"></i> Hapus</button>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    public function getDaftarKelainan(Request $request){
        $daftarKelainan = DaftarKelainan::where('id', $request->id_daftar_kelainan)->first();

        return response()->json([
            'success' => true,
            'message' => 'Success',
            'data' => $daftarKelainan
        ], 200);
    }

    public function addOrUpdateDaftarKelainan(Request $request){
        //validate form
        $validator = Validator::make($request->all(), [
            'nama_kelainan' => 'required',
            'id_kategori_penyakit' => 'required'
        ]);

        //validator check
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()
            ], 200);
        }
        
        $daftarKelainan = Array();
        $daftarKelainan['nama_kelainan'] = $request->nama_kelainan;
        
        if($request->form_mode == "add"){
            $daftarKelainan['id_kategori_penyakit'] = $request->id_kategori_penyakit;
            $insertOrUpdate = DaftarKelainan::create($daftarKelainan);
        }else{
            $insertOrUpdate = DaftarKelainan::where('id', $request->id_daftar_kelainan)->update($daftarKelainan);
        }

        if($insertOrUpdate){
            return response()->json([
                'success' => true,
                'message' => 'Kategori penyakit berhasil disimpan'
            ], 200);
        }else{
            return response()->json([
                'success' => false,
                'message' => 'Gagal menyimpan, silakan coba lagi'
            ], 200);
        }
    }

    public function deleteDaftarKelainan(Request $request){
        $delete = DaftarKelainan::where('id', $request->id_daftar_kelainan)->delete();
        
        return response()->json([
            'success' => true,
            'message' => 'Kategori penyakit berhasil dihapus!'
        ], 200);
    }
}