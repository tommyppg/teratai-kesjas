<?php
 
namespace App\Http\Controllers;
 
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\TKJ;
use DataTables;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use App\Helper\ImportHelper;
use Illuminate\Support\Facades\Hash;
 
class TKJController extends Controller implements WithCalculatedFormulas
{
    /**
     * Show the profile for a given user.
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function index()
    {  

        //for navigation activation
        $dataNav = array(
            "level1" => "tkj",
            "level2" => "tkj.import"
        );
        return view('tkj/import')
            ->with("dataNav", $dataNav);
    }

    public function import(Request $request){
        //validate form
        $validator = Validator::make($request->all(), [
            'excel' => 'required',
            'tahun_tkj' => 'required|numeric'
        ]);

        //validator check
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()
            ], 200);
        }

        //get imported file from request and tear into array
        $importExcel = Excel::toArray([], $request->file('excel'));
        //temp var
        $dataAnggota = array();
        $dataTKJ = array();

        //lets foreach
        foreach($importExcel[0] as $key => $dataExcel){
            if($dataExcel[0] != null && $dataExcel[1] != null){
                if($dataExcel[0] != "NO" && $dataExcel[1] != "NAMA"){
                    //add data to temp anggota
                    $dataAnggota[] = array(
                        'nama' => $dataExcel[1],
                        'role' => "anggota",
                        'nrp' => $dataExcel[3],
                        'pangkat' => $dataExcel[2],
                        'satker' => $dataExcel[4],
                        'jenis_kelamin' => $dataExcel[6],
                        'password' => Hash::make($dataExcel[3], [
                            'rounds' => 5,
                        ]),
                        // 'profile_status' => 'uncomplete',
                        // 'password_status' => 'uncomplete',
                        'status' => 'active'
                    );

                    // add data to temp rikkes
                    $dataTKJ[] = array(
                        'tahun_tkj' => $request->tahun_tkj,
                        'nrp' => $dataExcel[3],
                        'bb' => $dataExcel[7],
                        'tb' => $dataExcel[8],
                        'gol' => $dataExcel[9],
                        'samapta_a_hasil' => $dataExcel[10],
                        'samapta_a_nilai' => $dataExcel[11],
                        'samapta_b_pull_up_hasil' => $dataExcel[12],
                        'samapta_b_pull_up_nilai' => $dataExcel[13],
                        'samapta_b_sit_up_hasil' => $dataExcel[14],
                        'samapta_b_sit_up_nilai' => $dataExcel[15],
                        'samapta_b_push_up_hasil' => $dataExcel[16],
                        'samapta_b_push_up_nilai' => $dataExcel[17],
                        'samapta_b_shuttle_run_hasil' => $dataExcel[18],
                        'samapta_b_shuttle_run_nilai' => $dataExcel[19],
                        'nilai_rata_rata_b' => $dataExcel[20],
                        'nilai_akhir' => $dataExcel[21],
                        'kategori' => $dataExcel[22]
                    );
                }
            }
        }

        //insert to db user
        foreach (array_chunk($dataAnggota,1000) as $anggota)  {
            User::upsert($anggota, ['nrp']);
        }

        foreach (array_chunk($dataTKJ,1000) as $tkj)  {
            $queryState = TKJ::insertOrIgnore($tkj);
        }
        
        
        if($queryState) {
            return response()->json([
                'success' => true,
                'message' => "Import berhasil"
            ], 200);
        }else{
            return response()->json([
                'success' => false,
                'message' => "Import gagal"
            ], 200);
        }
    }

    public function lihat()
    {  
        //for navigation activation
        $dataNav = array(
            "level1" => "tkj",
            "level2" => "tkj.lihat"
        );
        return view('tkj/lihat')
            ->with("dataNav", $dataNav);
    }

    public function getData(Request $request)
    {  
        if ($request->ajax()) {
            // $data = Rikkes::join('users', 'rikkes.nrp', '=', 'users.nrp')
            //     ->latest('rikkes.created_at')
            //     ->get();
            $data = TKJ::select('tkj.*', 'users.nama', 'users.nrp')
                ->join('users', 'tkj.nrp', '=', 'users.nrp')
                ->latest('tkj.created_at')
                ->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('nilai', function($row){
                    switch($row->kategori){
                        case 'BS':
                            return '<span class="badge bg-primary text-white">Baik Sekali (BS)</span>';
                            break;
                        case 'B':
                            return '<span class="badge bg-success text-white">Baik (B)</span>';
                            break;
                        case 'C':
                            return '<span class="badge bg-info text-white">Cukup (C)</span>';
                            break;
                        case 'K':
                            return '<span class="badge bg-warning text-white">Kurang (K)</span>';
                            break;
                        case 'KS':
                            return '<span class="badge bg-dark text-white">Kurang Sekali (KS)</span>';
                            break;
                        default:
                            break;
                    }
                })
                ->addColumn('action', function($row){
                    $actionBtn = '<a href="'.url('anggota/detail/'.$row->nrp).'" class="edit btn btn-primary btn-sm"><i class="fas fa-eye"></i> Detail</a>';
                    return $actionBtn;
                })
                ->rawColumns(['action', 'nilai'])
                ->make(true);
        }
    }

    public function detail($nrp)
    {  
        //for navigation activation
        $dataNav = array(
            "level1" => "tkj",
            "level2" => "tkj.lihat"
        );

        $anggota = DB::table('users')->where('nrp', $nrp)->first();
        // dd($anggota);
        return view('tkj/detail')
            ->with("dataNav", $dataNav)
            ->with("dataAnggota", $anggota);
    }
}