@extends('layouts.main')

@section('css-page-spesific-plugin')
    <link rel="stylesheet" href="https://unpkg.com/dropzone@5/dist/min/dropzone.min.css" type="text/css" />
@endsection

@section('js-page-spesific-plugin')
    <script src="https://unpkg.com/dropzone@5/dist/min/dropzone.min.js"></script>
@endsection

@section('content')
    <h1 class="h3 mb-2 text-gray-800">Import Rikkes</h1>
    <p class="mb-4">Fitur ini adalah untuk melakukan import data dari Excel yang berasal dari aplikasi Rikkesla.</p>

    <div class="row">
        <div class="col-lg-8">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary"><i class="fas fa-file-excel"></i> Formulir Import Excel
                    </h6>
                </div>
                <div class="card-body">
                    <form action="{{ url('rikkes/import') }}" id="form-import-excel">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Ambil file excel</label>
                            <div id="my-dropzone" class="dropzone text-center"></div>
                        </div>
                        <div class="form-group">
                            <label for="tahun_rikkes">Tahun Rikkes</label>
                            <select name="tahun_rikkes" id="tahun_rikkes" class="form-control">
                                @for ($i = date('Y') + 2; $i >= date('Y') - 2; $i--)
                                    <option value="{{ $i }}" {{ date('Y') == $i ? 'selected' : '' }}>
                                        {{ $i }}</option>
                                @endfor
                            </select>
                        </div>
                        <button id="btn-submit" type="submit" class="btn btn-primary">Import</button>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary"><i class="fas fa-file-export"></i> Panduan Export Excel
                        dari Rikkesla</h6>
                </div>
                <div class="card-body">
                    <ol>
                        <li>Pada aplikasi Rikkesla, klik menu laporan</li>
                        <li>Pilih periode laporan (1 tahun)</li>
                        <li>Pilih save data ke Excel</li>
                    </ol>
                    <a href="{{ asset('file/FORMAT_RIKKES.xls') }}" class="btn btn-sm btn-success"><i class="fas fa-file-excel"></i> Unduh contoh Excel</a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js-page-custom')
    <script>
        Dropzone.autoDiscover = false;

        // Note that the name "myDropzone" is the camelized
        // id of the form.
        myDropzone = new Dropzone('div#my-dropzone', {
            url: "{{ url('rikkes/import') }}",
            autoProcessQueue: false,
            addRemoveLinks: true,
            acceptedFiles: ".xls,.xlsx",
            maxFiles: 1,
            dictDefaultMessage: '<i class="fas fa-plus-circle"></i> Klik untuk upload file Excel',

            init: function(e) {
                this.on("addedfile", file => {
                    $('.dz-progress').addClass('d-none');
                });

                this.on("maxfilesexceeded", function(file) {
                    this.removeFile(file);
                    alert("Hanya dapat mengimpor 1 file");
                });
            }
        });

        function getValues() {
            var formData = new FormData($('#form-import-excel')[0]);
            formData.append('excel', myDropzone.getAcceptedFiles()[0]);

            return formData;
        }

        $(document).on('submit', '#form-import-excel', function(e) {
            e.preventDefault();
            e.stopPropagation();

            $.ajax({
                method: 'POST',
                url: $('#form-import-excel').attr('action'),
                data: getValues(),
                processData: false, // required for FormData with jQuery
                contentType: false, // required for FormData with jQuery
                type: 'POST',
                method: 'POST',
                beforeSend: function() {
                    $("#btn-submit").prop("disabled", true);
                    $("#btn-submit").html('<i class="fas fa-spinner fa-pulse"></i> Import');
                },
                success: function(response) {
                    $("#btn-submit").prop("disabled", false);
                    $("#btn-submit").html('Import');
                    
                    if(response.success){
                        showToast("success", "Import Berhasil!", "Excel berhasil diimport ke dalam database.");

                        window.location = "{{ url('rikkes/lihat'); }}";
                    }else{
                        showToast("danger", "Import Gagal!", "Mohon dapat cek kembali format excel yang Anda input");
                    }
                }
            });
        });
    </script>
@endsection
