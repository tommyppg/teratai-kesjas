@extends('layouts.main')

@section('css-page-spesific-plugin')
    <link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection

@section('js-page-spesific-plugin')
    <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
@endsection

@section('content')
    <h1 class="h3 mb-2 text-gray-800">Detail Anggota</h1>
    <p class="mb-4">Data detail anggota serta riwayat rikkes</p>

    <div class="row">
        <div class="col-md-4">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Detail Anggota</h6>
                </div>
                <div class="card-body">
                    <table class="table table-bordered">
                        <tr>
                            <th>Nama</th>
                            <td>{{ $dataAnggota->nama }}</td>
                        </tr>
                        <tr>
                            <th>NRP</th>
                            <td>{{ $dataAnggota->nrp }}</td>
                        </tr>
                        <tr>
                            <th>Jenis Kelamin</th>
                            <td>
                                @if($dataAnggota->jenis_kelamin == "P")
                                Pria
                                @elseif($dataAnggota->jenis_kelamin == "W")
                                Wanita
                                @else
                                <em>Belum diisi</em>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Satker</th>
                            <td>{{ $dataAnggota->satker }}</td>
                        </tr>
                        <tr>
                            <th>Pangkat</th>
                            <td>{{ $dataAnggota->pangkat }}</td>
                        </tr>
                        <tr>
                            <th>Gol. Darah</th>
                            <td>
                                @if(!empty($dataAnggota->gol_darah))
                                    {{ $dataAnggota->gol_darah }}
                                @else
                                <em>Belum diisi</em>
                                @endif
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-md-8">
            <!-- DataTales Example -->
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Riwayat Rikkes</h6>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    {{-- <th>No</th> --}}
                                    <th>Tahun Rikkes</th>
                                    <th>Intensif</th>
                                    <th>Kelainan</th>
                                    <th>Nilai</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js-page-custom')
    <script>
        // Call the dataTables jQuery plugin
        $(document).ready(function() {

            var table = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ url('tindakan/get-data-rikkes/'.$dataAnggota->nrp) }}",
                columns: [
                    // {
                    //     data: 'DT_RowIndex',
                    //     name: 'DT_RowIndex'
                    // },
                    {
                        data: 'tahun_rikkes',
                        name: 'tahun_rikkes',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'intensif',
                        name: 'intensif',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'kelainan',
                        name: 'kelainan',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'kualitas',
                        name: 'kualitas',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'action',
                        name: 'action',
                    },
                ],
                "rowCallback": function(row, data) {
                    switch(data.kualitas){
                        case "Baik (B)":
                            $('td:eq(3)', row).html('<span class="badge bg-success text-white">'+data.kualitas+'</span>');
                            break;
                        case "Cukup (C)":
                            $('td:eq(3)', row).html('<span class="badge bg-info text-white">'+data.kualitas+'</span>');
                            break;
                        case "Kurang (K1)":
                            $('td:eq(3)', row).html('<span class="badge bg-warning text-white">'+data.kualitas+'</span>');
                            break;
                            case "Kurang Sekali (K2)":
                            $('td:eq(3)', row).html('<span class="badge bg-dark text-white">'+data.kualitas+'</span>');
                            break;
                        default:
                            break;
                    }
                }
            });
        });
    </script>
@endsection
