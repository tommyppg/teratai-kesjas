@extends('layouts/main')

@section('js-page-spesific-plugin')
    <script src="{{ asset('vendor/chart.js/Chart.min.js') }}"></script>
@endsection

@section('content')
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
        {{-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> --}}
    </div>

    <!-- Content Row -->
    <div class="row">
        @if(Auth::user()->role == "superadmin")
        <div class="col-lg-6 mb-4">
            <div class="card bg-success text-white shadow">
                <div class="card-body">
                    <div class="text-white small">
                        <i class="fas fa-check"></i> Aktivasi Aplikasi Teratai Kesjas Mobile
                    </div>
                    <div class="h5 mb-0 font-weight-bold text-white">
                        {{ number_format($dataAktivasiUser['active'], 0, ',', '.') }}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 mb-4">
            <div class="card bg-dark text-white shadow">
                <div class="card-body">
                    <div class="text-white small">
                        <i class="fas fa-times"></i> Belum Aktivasi
                    </div>
                    <div class="h5 mb-0 font-weight-bold text-white">
                        {{ number_format($dataAktivasiUser['inactive'], 0, ',', '.') }}
                    </div>
                </div>
            </div>
        </div>
        @endif
        <!-- Total Rikkes Card -->
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                Total Data Rikkes</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">
                                {{ number_format($dataCountSummary['countAll'], 0, ',', '.') }}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                INTENSIF 1</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">
                                {{ number_format($dataCountSummary['countIntensif1'], 0, ',', '.') }}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-user-check fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">INTENSIF 2
                            </div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">
                                {{ number_format($dataCountSummary['countIntensif2'], 0, ',', '.') }}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-user-check fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Pending Requests Card Example -->
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                INTENSIF 3</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">
                                {{ number_format($dataCountSummary['countIntensif3'], 0, ',', '.') }}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-user-check fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Content Row -->
    <div class="row">
        <div class="col-xl-12 col-lg-12">
            <!-- Bar Chart -->
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Grafik Kualitas Hasil Rikkes</h6>
                </div>
                <div class="card-body">
                    <div class="chart-bar">
                        <canvas id="myBarChart"></canvas>
                    </div>
                    {{-- <hr>
                    Styling for the bar chart can be found in the
                    <code>/js/demo/chart-bar-demo.js</code> file. --}}
                </div>
            </div>
        </div>

        <div class="col-xl-12 col-lg-12">
            <!-- Bar Chart -->
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Grafik Kualitas Hasil TKJ</h6>
                </div>
                <div class="card-body">
                    <div class="chart-bar">
                        <canvas id="myBarChart2"></canvas>
                    </div>
                    {{-- <hr>
                    Styling for the bar chart can be found in the
                    <code>/js/demo/chart-bar-demo.js</code> file. --}}
                </div>
            </div>
        </div>

        <div class="col-xl-6 col-lg-6">
            <!-- Bar Chart -->
            <div class="card shadow mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Presentase Penyakit/Kelainan</h6>

                    {{-- <span class="badge badge-primary btn-refresh-kelainan" style="cursor: pointer;">Refresh</span> --}}
                </div>
                <div class="card-body">
                    @if(count($dataGraphKelainan['all']) == 0)
                        <em>Data penyakit/kelainan belum dihitung, silakan klik tombol refresh di atas untuk menghitung</em>
                    @else
                    <div class="chart-pie">
                        <canvas id="myPieChart"></canvas>
                    </div>
                    @endif
                    {{-- <hr>
                    Styling for the bar chart can be found in the
                    <code>/js/demo/chart-bar-demo.js</code> file. --}}
                </div>
            </div>
        </div>

        <div class="col-xl-6 col-lg-6">
            <!-- Bar Chart -->
            <div class="card shadow mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Penyakit/Kelainan Terbanyak</h6>

                    {{-- <span class="badge badge-primary btn-refresh-kelainan" style="cursor: pointer;">Refresh</span> --}}
                </div>
                <div class="card-body">
                    @if(count($dataGraphKelainan['all']) == 0)
                        <em>Data penyakit/kelainan belum dihitung, silakan klik tombol refresh di atas untuk menghitung</em>
                    @else
                    <table class="table table-bordered">
                        <thead>
                            <th>No.</th>
                            <th>Nama Penyakit/Kelainan</th>
                            <th>Total Ditemukan</th>
                        </thead>
                        <tbody>
                            @foreach($dataGraphKelainan['topKelainan'] as $key => $kelainan)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $kelainan->nama_kelainan }}</td>
                                    <td>{{ $kelainan->total }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js-page-custom')
    <script>
        // Set new default font family and font color to mimic Bootstrap's default styling
        Chart.defaults.global.defaultFontFamily = 'Nunito',
            '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
        Chart.defaults.global.defaultFontColor = '#858796';

        function number_format(number, decimals, dec_point, thousands_sep) {
            // *     example: number_format(1234.56, 2, ',', ' ');
            // *     return: '1 234,56'
            number = (number + '').replace(',', '').replace(' ', '');
            var n = !isFinite(+number) ? 0 : +number,
                prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
                sep = (typeof thousands_sep === 'undefined') ? '.' : thousands_sep,
                dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
                s = '',
                toFixedFix = function(n, prec) {
                    var k = Math.pow(10, prec);
                    return '' + Math.round(n * k) / k;
                };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '').length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1).join('0');
            }
            return s.join(dec);
        }

        // Bar Chart Example
        var ctx = document.getElementById("myBarChart");
        var myBarChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ["Baik (B)", "Cukup (C)", "Kurang (K1)", "Kurang Sekali (K2)"],
                datasets: [{
                    label: "Jumlah Anggota",
                    backgroundColor: ["#1cc88a", "#36b9cc", "#f6c23e", "#e74a3b"],
                    hoverBackgroundColor: ["#1cc88a", "#36b9cc", "#f6c23e", "#e74a3b"],
                    borderColor: "#4e73df",
                    data: [{{ $dataGraphNilai['countNilaiB'] }}, {{ $dataGraphNilai['countNilaiC'] }},
                        {{ $dataGraphNilai['countNilaiK1'] }}, {{ $dataGraphNilai['countNilaiK2'] }}
                    ],
                }],
            },
            options: {
                maintainAspectRatio: false,
                layout: {
                    padding: {
                        left: 10,
                        right: 25,
                        top: 25,
                        bottom: 0
                    }
                },
                scales: {
                    xAxes: [{
                        time: {
                            unit: 'Kualitas'
                        },
                        gridLines: {
                            display: false,
                            drawBorder: false
                        },
                        ticks: {
                            maxTicksLimit: 4
                        },
                        maxBarThickness: 25,
                    }],
                    yAxes: [{
                        ticks: {
                            min: 0,
                            max: {{ max($dataGraphNilai) * 1.3 }},
                            maxTicksLimit: 5,
                            padding: 10,
                            // Include a dollar sign in the ticks
                            callback: function(value, index, values) {
                                // return '$' + number_format(value);
                                return number_format(value);
                            }
                        },
                        gridLines: {
                            color: "rgb(234, 236, 244)",
                            zeroLineColor: "rgb(234, 236, 244)",
                            drawBorder: false,
                            borderDash: [2],
                            zeroLineBorderDash: [2]
                        }
                    }],
                },
                legend: {
                    display: false
                },
                tooltips: {
                    titleMarginBottom: 10,
                    titleFontColor: '#6e707e',
                    titleFontSize: 14,
                    backgroundColor: "rgb(255,255,255)",
                    bodyFontColor: "#858796",
                    borderColor: '#dddfeb',
                    borderWidth: 1,
                    xPadding: 15,
                    yPadding: 15,
                    displayColors: false,
                    caretPadding: 10,
                    callbacks: {
                        label: function(tooltipItem, chart) {
                            var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
                            // return datasetLabel + ': $' + number_format(tooltipItem.yLabel);
                            return datasetLabel + ': ' + number_format(tooltipItem.yLabel);
                        }
                    }
                },
            }
        });

        // Bar Chart Example
        var ctx2 = document.getElementById("myBarChart2");
        var myBarChart2 = new Chart(ctx2, {
            type: 'bar',
            data: {
                labels: ["Baik Sekali (BS)", "Baik (B)", "Cukup (C)", "Kurang Sekali (KS)"],
                datasets: [{
                    label: "Jumlah Anggota",
                    backgroundColor: ["#1cc88a", "#36b9cc", "#f6c23e", "#e74a3b"],
                    hoverBackgroundColor: ["#1cc88a", "#36b9cc", "#f6c23e", "#e74a3b"],
                    borderColor: "#4e73df",
                    data: [{{ $dataGraphNilaiTKJ['countNilaiBS'] }}, {{ $dataGraphNilaiTKJ['countNilaiB'] }},
                        {{ $dataGraphNilaiTKJ['countNilaiC'] }}, {{ $dataGraphNilaiTKJ['countNilaiKS'] }}
                    ],
                }],
            },
            options: {
                maintainAspectRatio: false,
                layout: {
                    padding: {
                        left: 10,
                        right: 25,
                        top: 25,
                        bottom: 0
                    }
                },
                scales: {
                    xAxes: [{
                        time: {
                            unit: 'Kualitas'
                        },
                        gridLines: {
                            display: false,
                            drawBorder: false
                        },
                        ticks: {
                            maxTicksLimit: 4
                        },
                        maxBarThickness: 25,
                    }],
                    yAxes: [{
                        ticks: {
                            min: 0,
                            max: {{ max($dataGraphNilaiTKJ) * 1.3 }},
                            maxTicksLimit: 5,
                            padding: 10,
                            // Include a dollar sign in the ticks
                            callback: function(value, index, values) {
                                // return '$' + number_format(value);
                                return number_format(value);
                            }
                        },
                        gridLines: {
                            color: "rgb(234, 236, 244)",
                            zeroLineColor: "rgb(234, 236, 244)",
                            drawBorder: false,
                            borderDash: [2],
                            zeroLineBorderDash: [2]
                        }
                    }],
                },
                legend: {
                    display: false
                },
                tooltips: {
                    titleMarginBottom: 10,
                    titleFontColor: '#6e707e',
                    titleFontSize: 14,
                    backgroundColor: "rgb(255,255,255)",
                    bodyFontColor: "#858796",
                    borderColor: '#dddfeb',
                    borderWidth: 1,
                    xPadding: 15,
                    yPadding: 15,
                    displayColors: false,
                    caretPadding: 10,
                    callbacks: {
                        label: function(tooltipItem, chart) {
                            var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
                            // return datasetLabel + ': $' + number_format(tooltipItem.yLabel);
                            return datasetLabel + ': ' + number_format(tooltipItem.yLabel);
                        }
                    }
                },
            }
        });

        @if(count($dataGraphKelainan['all']) != 0)
        // Set new default font family and font color to mimic Bootstrap's default styling
        Chart.defaults.global.defaultFontFamily = 'Nunito',
            '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
        Chart.defaults.global.defaultFontColor = '#858796';

        var coloR = [];

        var dynamicColors = function() {
            var r = Math.floor(Math.random() * 255);
            var g = Math.floor(Math.random() * 255);
            var b = Math.floor(Math.random() * 255);
            return "rgb(" + r + "," + g + "," + b + ")";
        };
        @foreach($dataGraphKelainan['all'] as $kelainan)
            coloR.push(dynamicColors());
        @endforeach

        // Pie Chart Example
        var ctx = document.getElementById("myPieChart");
        var myPieChart = new Chart(ctx, {
            type: 'doughnut',
            data: {
                labels: [
                    @foreach($dataGraphKelainan['all'] as $kelainan)
                        "{{ $kelainan->nama_kelainan }}",
                    @endforeach
                ],
                datasets: [{
                    data: [
                        @foreach($dataGraphKelainan['all'] as $kelainan)
                            "{{ $kelainan->total }}",
                        @endforeach
                    ],
                    backgroundColor: coloR
                }],
            },
            options: {
                maintainAspectRatio: false,
                tooltips: {
                    backgroundColor: "rgb(255,255,255)",
                    bodyFontColor: "#858796",
                    borderColor: '#dddfeb',
                    borderWidth: 1,
                    xPadding: 10,
                    yPadding: 10,
                    displayColors: false,
                    caretPadding: 10,
                },
                legend: {
                    display: false
                },
                cutoutPercentage: 0,
            },
        });
        @endif

        $('.btn-refresh-kelainan').click(function(){
            window.location.href = "{{ url('dashboard/calculate-kelainan') }}";
        });
    </script>
@endsection
