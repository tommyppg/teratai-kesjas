@extends('layouts.main')

@section('css-page-spesific-plugin')
    <link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection

@section('js-page-spesific-plugin')
    <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="https://cdn.tiny.cloud/1/ka5n7skzy7c4mgr5ni6g84uvmmmak91ca78vgwmijz5kog0o/tinymce/5/tinymce.min.js"
        referrerpolicy="origin"></script>
@endsection

@section('content')
    <h1 class="h3 mb-2 text-gray-800">Detail Tindakan</h1>
    <p class="mb-4">Data detail tindakan pada suatu hasil Rikkes</p>

    <div class="row">
        <div class="col-md-12">
            <!-- DataTales Example -->
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Data Rikkes</h6>
                </div>
                <div class="card-body">
                    <table class="table table-bordered table-responsive">
                        <thead>
                            <tr class="bg-info text-white">
                                <th>Nama</th>
                                <th>NRP</th>
                                <th>Jenis Kelamin</th>
                                <th>Satker</th>
                                <th>Pangkat</th>
                                <th>Tahun Rikkes</th>
                                <th>Intensif</th>
                                <th>Kelainan</th>
                                <th>Nilai</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{ $dataAnggota->nama }}</td>
                                <td>{{ $dataAnggota->nrp }}</td>
                                <td>
                                    @if ($dataAnggota->jenis_kelamin == 'P')
                                        Pria
                                    @elseif($dataAnggota->jenis_kelamin == 'W')
                                        Wanita
                                    @else
                                        <em>Belum diisi</em>
                                    @endif
                                </td>
                                <td>{{ $dataAnggota->satker }}</td>
                                <td>{{ $dataAnggota->pangkat }}</td>
                                <td>{{ $dataRikkes->tahun_rikkes }}</td>
                                <td>{{ $dataRikkes->intensif }}</td>
                                <td>{{ $dataRikkes->kelainan }}</td>
                                <td>{{ $dataRikkes->kualitas }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">
            <!-- DataTales Example -->
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Riwayat Tindakan</h6>
                </div>
                <div class="card-body">
                    @if(Auth::user()->role == "fktp" || Auth::user()->role == "rs" || Auth::user()->role == "rs_poli")
                    <button type="button" class="btn btn-primary float-end" id="btn-show-form-tindakan"><i
                            class="fas fa-plus-circle"></i> Tambah Tindakan</button>
                    <br>
                    <br>
                    @endif
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    {{-- <th>No</th> --}}
                                    <th>Tanggal</th>
                                    <th>Tipe Tindakan</th>
                                    <th>Jenis Tindakan</th>
                                    <th>Nama Faskes</th>
                                    <th>Tindakan</th>
                                    <th>Rujukan</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Form Tindakan Modal-->
    <div class="modal fade" id="modal-form-tindakan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <form action="{{ url('tindakan/add-or-update-tindakan') }}" id="form-tindakan">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Tambah Tindakan</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="deskripsi_tindakan">Jenis Tindakan</label>
                            <select class="form-control" name="jenis" id="jenis">
                                <option value="">-- PILIH JENIS TINDAKAN --</option>
                                <option value="tindakan_akhir">Tindakan Akhir</option>
                                <option value="butuh_rujukan">Butuh Rujukan</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="deskripsi_tindakan">Tindakan</label>
                            <textarea class="form-control" name="deskripsi_tindakan" id="deskripsi_tindakan" cols="30" rows="10"></textarea>
                        </div>
                        <div class="form-group" id="keterangan_rujukan_box" style="display: none">
                            <label for="keterangan_rujukan">Poli Rujukan</label>
                                @foreach($jenisPoli as $poli)
                                <div class="form-check">
                                    <input name="poli_dituju[]" class="form-check-input" type="checkbox" value="{{ $poli->id }}" id="poli_dituju_{{ $poli->id }}">
                                    <label class="form-check-label" for="poli_dituju_{{ $poli->id }}">
                                        {{ $poli->nama_jenis_poli }}
                                    </label>
                                </div>
                                @endforeach
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="id_rikkes" id="id_rikkes" value="{{ $dataRikkes->id }}">
                        <input type="hidden" name="id_tindakan" id="id_tindakan" value="">
                        <input type="hidden" name="form_mode" id="form_mode" value="create">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary" id="btn-submit">Simpan</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('js-page-custom')
    <script>
        tinymce.init({
            selector: 'textarea',
            toolbar_mode: 'floating',
            plugins: 'lists',
            menubar: false,
            toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | numlist bullist',
            tinycomments_mode: 'embedded',
            tinycomments_author: 'Author name',
        });
        // Call the dataTables jQuery plugin
        $(document).ready(function() {
            $('#btn-show-form-tindakan').click(function() {
                $('#modal-form-tindakan').modal({
                    backdrop: 'static',
                    keyboard: false
                }, 'show');

                $('#form_mode').val('add');
                $('#id_tindakan').val('');
                $('#jenis').val('');
                tinyMCE.get('deskripsi_tindakan').setContent('');
            });

            var table = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ url('tindakan/get-data-tindakan/' . $dataRikkes->id) }}",
                columns: [
                    // {
                    //     data: 'DT_RowIndex',
                    //     name: 'DT_RowIndex'
                    // },
                    {
                        data: 'created_at',
                        name: 'created_at',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'tipe',
                        name: 'tipe',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'jenis',
                        name: 'jenis',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'nama_satker',
                        name: 'nama_satker',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'tindakan',
                        name: 'tindakan',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'poli_dituju',
                        name: 'poli_dituju',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'status',
                        name: 'status',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'action',
                        name: 'action',
                    },
                ],
                "rowCallback": function(row, data) {
                    switch (data.kualitas) {
                        case "Baik (B)":
                            $('td:eq(3)', row).html('<span class="badge bg-success text-white">' + data
                                .kualitas + '</span>');
                            break;
                        case "Cukup (C)":
                            $('td:eq(3)', row).html('<span class="badge bg-info text-white">' + data
                                .kualitas + '</span>');
                            break;
                        case "Kurang (K1)":
                            $('td:eq(3)', row).html('<span class="badge bg-warning text-white">' + data
                                .kualitas + '</span>');
                            break;
                        case "Kurang Sekali (K2)":
                            $('td:eq(3)', row).html('<span class="badge bg-dark text-white">' + data
                                .kualitas + '</span>');
                            break;
                        default:
                            break;
                    }
                }
            });

            $('#jenis').on('change', function(){
                if($(this).val() == "butuh_rujukan"){
                    $('#keterangan_rujukan_box').show();
                }else{
                    $('#keterangan_rujukan_box').hide();
                }
            });

            $(document).on('submit', '#form-tindakan', function(e) {
                e.preventDefault();
                e.stopPropagation();

                var editorContent = $.trim(tinyMCE.get('deskripsi_tindakan').getContent({
                    format: 'text'
                }));

                if (editorContent != 0) {
                    $.ajax({
                        url: $('#form-tindakan').attr('action'),
                        data: $('#form-tindakan').serializeArray(),
                        method: 'POST',
                        beforeSend: function() {
                            $("#btn-submit").prop("disabled", true);
                            $("#btn-submit").html(
                                '<i class="fas fa-spinner fa-pulse"></i> Simpan');
                        },
                        success: function(response) {
                            if (response.success) {
                                //hide modal
                                $('#modal-form-tindakan').modal('hide');

                                //reload ajax
                                table.ajax.reload();

                                //show toast
                                showToast("success", "Berhasil", response.message);

                                //reset form
                                tinyMCE.get('deskripsi_tindakan').setContent('');
                                $("#btn-submit").prop("disabled", false);
                                $("#btn-submit").html('Simpan');
                                // setTimeout(function(){
                                //     location.reload();
                                // }, 1000);
                            } else {
                                showToast("danger", "Gagal!", response.message);
                            }
                        }
                    });
                } else {
                    showToast("danger", "Gagal!", "Tindakan tidak boleh kosong");
                }
            });

            $(document).on('click', '.btn-edit-tindakan', function() {
                var thisButton = $(this);

                var id_tindakan = thisButton.data('idTindakan');

                $.ajax({
                    url: '{{ url('tindakan/get-tindakan') }}',
                    data: {
                        id_tindakan: id_tindakan
                    },
                    method: 'GET',
                    beforeSend: function() {
                        thisButton.prop("disabled", true);
                        thisButton.html('<i class="fas fa-spinner fa-pulse"></i> Loading');
                    },
                    success: function(response) {
                        thisButton.prop("disabled", false);
                        thisButton.html('<i class="fas fa-edit"></i> Update');

                        if (response.data != null) {
                            tinyMCE.get('deskripsi_tindakan').setContent(response.data
                            .tindakan);
                            $('#form_mode').val('update');
                            $('#id_tindakan').val(response.data.id);
                            $('#jenis').val(response.data.jenis);

                            $('#modal-form-tindakan').modal({
                                backdrop: 'static',
                                keyboard: false
                            }, 'show');
                        } else {

                        }

                    }
                });
            });

            $(document).on('click', '.btn-hapus-tindakan', function() {
                var thisButton = $(this);

                var id_tindakan = thisButton.data('idTindakan');

                if (confirm("Apakah Anda yakin akan menghapus data ini?")) {
                    $.ajax({
                        url: '{{ url('tindakan/delete-tindakan') }}',
                        data: {
                            id_tindakan: id_tindakan
                        },
                        method: 'GET',
                        beforeSend: function() {
                            thisButton.prop("disabled", true);
                            thisButton.html('<i class="fas fa-spinner fa-pulse"></i> Loading');
                        },
                        success: function(response) {
                            if(response.success){
                                //reload ajax
                                table.ajax.reload();

                                //show toast
                                showToast("success", "Berhasil", response.message);
                            }else{
                                showToast("danger", "Gagal!", "Silakan coba lagi.");
                            }
                        }
                    });
                }
                return false;
            });
        });
    </script>
@endsection
