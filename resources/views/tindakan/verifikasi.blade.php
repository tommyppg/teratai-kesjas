@extends('layouts.main')

@section('css-page-spesific-plugin')
    <link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection

@section('js-page-spesific-plugin')
    <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="https://cdn.tiny.cloud/1/ka5n7skzy7c4mgr5ni6g84uvmmmak91ca78vgwmijz5kog0o/tinymce/5/tinymce.min.js"
        referrerpolicy="origin"></script>
@endsection

@section('content')
    <h1 class="h3 mb-2 text-gray-800">Verifikasi Tindakan Pribadi</h1>
    <p class="mb-4">Fitur untuk memverifikasi tindakan pribadi anggota</p>
    
    <div class="row">
        <div class="col-md-12">
            <!-- DataTales Example -->
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Riwayat Tindakan</h6>
                </div>
                <div class="card-body">
                    @if(Auth::user()->role == "fktp" || Auth::user()->role == "rs" || Auth::user()->role == "rs_poli")
                    <button type="button" class="btn btn-primary float-end" id="btn-show-form-tindakan"><i
                            class="fas fa-plus-circle"></i> Tambah Tindakan</button>
                    <br>
                    <br>
                    @endif
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    {{-- <th>No</th> --}}
                                    <th>Tanggal</th>
                                    <th>Nama</th>
                                    <th>NRP</th>
                                    <th>Satker</th>
                                    <th>Nama Faskes</th>
                                    <th>Poli</th>
                                    <th>Tindakan</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Form Tindakan Modal-->
    <div class="modal fade" id="modal-form-verifikasi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <form action="{{ url('tindakan/verifikasi-process') }}" id="form-verifikasi">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Verifikasi</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h4>Nomor Pendaftaran</h4>
                        <div id="nomor_pendaftaran_box"></div>
                        <br>
                        <h4>Foto Resep</h4>
                        <div id="foto_resep_box"></div>
                        <br>
                        <h4>Foto Obat</h4>
                        <div id="foto_obat_box"></div>
                        <br>
                        <h4>Foto Kwitansi</h4>
                        <div id="foto_kwitansi_box"></div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="id_tindakan" id="id_tindakan" value="">
                        <input type="hidden" name="id_jenis_poli" id="id_jenis_poli" value="">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary" id="btn-submit">Verifikasi</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('js-page-custom')
    <script>
        // Call the dataTables jQuery plugin
        $(document).ready(function() {
            $('#btn-show-form-tindakan').click(function() {
                $('#modal-form-tindakan').modal({
                    backdrop: 'static',
                    keyboard: false
                }, 'show');

                $('#form_mode').val('add');
            });

            var table = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ url('tindakan/get-data-verifikasi') }}",
                columns: [
                    // {
                    //     data: 'DT_RowIndex',
                    //     name: 'DT_RowIndex'
                    // },
                    {
                        data: 'created_at',
                        name: 'created_at',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'nama',
                        name: 'nama',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'nrp',
                        name: 'nrp',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'satker',
                        name: 'satker',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'nama_satker',
                        name: 'nama_satker',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'poli_dituju',
                        name: 'poli_dituju',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'tindakan',
                        name: 'tindakan',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'action',
                        name: 'action',
                    },
                ]
            });

            $('#jenis').on('change', function(){
                if($(this).val() == "butuh_rujukan"){
                    $('#keterangan_rujukan_box').show();
                }else{
                    $('#keterangan_rujukan_box').hide();
                }
            });

            $(document).on('submit', '#form-verifikasi', function(e) {
                e.preventDefault();
                e.stopPropagation();

                $.ajax({
                    url: $('#form-verifikasi').attr('action'),
                    data: $('#form-verifikasi').serializeArray(),
                    method: 'POST',
                    beforeSend: function() {
                        $("#btn-submit").prop("disabled", true);
                        $("#btn-submit").html(
                            '<i class="fas fa-spinner fa-pulse"></i> Verifikasi');
                    },
                    success: function(response) {
                        $("#btn-submit").prop("disabled", false);
                        $("#btn-submit").html(
                            '<i class="fas fa-check"></i> Verifikasi');

                        if (response.success) {
                            //hide modal
                            $('#modal-form-verifikasi').modal('hide');

                            //reload ajax
                            table.ajax.reload();

                            //show toast
                            showToast("success", "Berhasil", response.message);
                        } else {
                            showToast("danger", "Gagal!", response.message);
                        }
                    }
                });
            });

            $(document).on('click', '.btn-verifikasi', function() {
                var thisButton = $(this);

                var nomor_pendaftaran_name = thisButton.data('nomorPendaftaran');
                if(nomor_pendaftaran_name){
                    $('#nomor_pendaftaran_box').html('<strong>'+nomor_pendaftaran_name+'</strong>');
                }else{
                    $('#nomor_pendaftaran_box').html('Tidak mengirimkan nomor pendaftaran');
                }

                var foto_resep_name = thisButton.data('fotoResep');
                if(foto_resep_name){
                    $('#foto_resep_box').html('<img width="100%" src="'+foto_resep_name+'"/>');
                }else{
                    $('#foto_resep_box').html('Tidak mengirimkan foto resep.');
                }

                var foto_obat_name = thisButton.data('fotoObat');
                if(foto_obat_name){
                    $('#foto_obat_box').html('<img width="100%" src="'+foto_obat_name+'"/>');
                }else{
                    $('#foto_obat_box').html('Tidak mengirimkan foto obat.');
                }

                var foto_kwitansi_name = thisButton.data('fotoKwitansi');
                if(foto_kwitansi_name){
                    $('#foto_kwitansi_box').html('<img width="100%" src="'+foto_kwitansi_name+'"/>');
                }else{
                    $('#foto_kwitansi_box').html('Tidak mengirimkan foto kwitansi.');
                }

                var id_tindakan = thisButton.data('idTindakan');
                $('#id_tindakan').val(id_tindakan);

                $('#modal-form-verifikasi').modal({
                    backdrop: 'static',
                    keyboard: false
                }, 'show');
            });

            $(document).on('click', '.btn-hapus-tindakan', function() {
                var thisButton = $(this);

                var id_tindakan = thisButton.data('idTindakan');

                if (confirm("Apakah Anda yakin akan menghapus data ini?")) {
                    $.ajax({
                        url: '{{ url('tindakan/delete-tindakan') }}',
                        data: {
                            id_tindakan: id_tindakan
                        },
                        method: 'GET',
                        beforeSend: function() {
                            thisButton.prop("disabled", true);
                            thisButton.html('<i class="fas fa-spinner fa-pulse"></i> Loading');
                        },
                        success: function(response) {
                            if(response.success){
                                //reload ajax
                                table.ajax.reload();

                                //show toast
                                showToast("success", "Berhasil", response.message);
                            }else{
                                showToast("danger", "Gagal!", "Silakan coba lagi.");
                            }
                        }
                    });
                }
                return false;
            });
        });
    </script>
@endsection
