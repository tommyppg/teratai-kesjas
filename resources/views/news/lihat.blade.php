@extends('layouts.main')

@section('css-page-spesific-plugin')
    <link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection

@section('js-page-spesific-plugin')
    <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendor/jquery-validation-1.19.5/dist/jquery.validate.js') }}"></script>
    <script src="https://cdn.tiny.cloud/1/ka5n7skzy7c4mgr5ni6g84uvmmmak91ca78vgwmijz5kog0o/tinymce/5/tinymce.min.js"
        referrerpolicy="origin"></script>
@endsection

@section('content')
    <h1 class="h3 mb-2 text-gray-800">Lihat Data News</h1>
    <p class="mb-4">Broadcast berita dan pesan ke Anggota</p>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Data News</h6>
        </div>
        <div class="card-body">
            <button type="button" class="btn btn-primary float-end" id="btn-show-form-news"><i
                class="fas fa-plus-circle"></i> Tambah Berita</button>
            <br>
            <br>
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            {{-- <th>No</th> --}}
                            <th>Tanggal</th>
                            <th>Judul</th>
                            <th>Dibaca</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tfoot>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- Form News Modal-->
    <div class="modal fade" id="modal-form-news" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <form action="{{ url('news/add-or-update-news') }}" id="form-news">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Tambah News</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="judul">Judul</label>
                            <input type="text" class="form-control" name="judul" id="judul">
                        </div>
                        <div class="form-group">
                            <label for="isi">Isi</label>
                            <textarea class="form-control" name="isi" id="isi" cols="30" rows="10"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="isi">Lampiran</label>
                            <input type="file" name="lampiran" class="form-control" id="lampiran">
                            <a href="javascript:;" id="btn-download-lampiran" style="display: none;" target="_blank"><i class="fas fa-cloud-download-alt"></i> Unduh lampiran</a>
                        </div>
                        <div class="form-group">
                            <label for="status">Publikasikan?</label>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="status" id="status_published" value="published" checked>
                                <label class="form-check-label" for="status_published">
                                  Publikasikan
                                </label>
                              </div>
                              <div class="form-check">
                                <input class="form-check-input" type="radio" name="status" value="unpublished" id="status_unpublished">
                                <label class="form-check-label" for="status_unpublished">
                                  Draft
                                </label>
                              </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="id_news" id="id_news" value="">
                        <input type="hidden" name="form_mode" id="form_mode" value="add">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary" id="btn-submit">Simpan</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('js-page-custom')
    <script>
        tinymce.init({
            selector: 'textarea',
            toolbar_mode: 'floating',
            plugins: 'lists',
            menubar: false,
            toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | numlist bullist',
            tinycomments_mode: 'embedded',
            tinycomments_author: 'Author name',
        });

        // Call the dataTables jQuery plugin
        $(document).ready(function() {

            var table = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ url('news/get-data-news') }}",
                columns: [
                    // {
                    //     data: 'DT_RowIndex',
                    //     name: 'DT_RowIndex'
                    // },
                    {
                        data: 'created_at',
                        name: 'created_at',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'judul',
                        name: 'judul',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'jumlah_readers',
                        name: 'jumlah_readers',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'status',
                        name: 'status',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'action',
                        name: 'action',
                    },
                ]
            });

            var validator = $("#form-news").validate({
                rules: {
                    judul: {
                        required: true
                    }
                },
                messages: {
                    judul: {
                        required: "Mohon masukkan judul"
                    }
                },
                errorElement: "div",
                errorClass: 'invalid-feedback d-block',
                errorPlacement: function(error, element) {
                    if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.parent("label"));
                    } else {
                        error.insertAfter(element);
                    }
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).removeClass("is-valid");
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-valid");
                },
                submitHandler: function(form) {
                    var formData = new FormData(form);

                    $.ajax({
                        url: $('#form-news').attr('action'),
                        data: formData,
                        method: 'POST',
                        cache: false,
                        contentType: false,
                        processData: false,
                        beforeSend: function() {
                            $("#btn-submit").prop("disabled", true);
                            $("#btn-submit").html(
                                '<i class="fas fa-spinner fa-pulse"></i> Simpan');
                        },
                        success: function(response) {
                            //reset form
                            $("#btn-submit").prop("disabled", false);
                            $("#btn-submit").html('Simpan');

                            if (response.success) {
                                //hide modal
                                $('#modal-form-news').modal('hide');

                                //reload ajax
                                table.ajax.reload();

                                //show toast
                                showToast("success", "Berhasil", response.message);
                            } else {
                                var messages = '';
                                $.each(response.message, function (index, value) {
                                    messages = messages + value + "<br>";
                                });

                                showToast("danger", "Gagal!", messages);
                            }
                        }
                    });
                    return false; // required to block normal submit since you used ajax
                }
            });

            $('#btn-show-form-news').click(function() {
                $('#modal-form-news').modal({
                    backdrop: 'static',
                    keyboard: false
                }, 'show');

                $('#form_mode').val('add');
                $('#judul').val('');
                $('#lampiran').val('');
                tinyMCE.get('isi').setContent('');
                $("#status_published").prop("checked", true);

                $('.is-valid').removeClass('is-valid');
                validator.resetForm();
            });

            $(document).on('click', '.btn-update-news', function() {
                var thisButton = $(this);

                var id_news = thisButton.data('idNews');

                $.ajax({
                    url: '{{ url('news/get-news') }}',
                    data: {
                        id_news: id_news
                    },
                    method: 'GET',
                    beforeSend: function() {
                        thisButton.prop("disabled", true);
                        thisButton.html('<i class="fas fa-spinner fa-pulse"></i> Loading');
                    },
                    success: function(response) {
                        thisButton.prop("disabled", false);
                        thisButton.html('<i class="fas fa-edit"></i> Update');
                        
                        if (response.data != null) {
                            $('#lampiran').val('');
                            $('#form_mode').val('update');
                            $('#judul').val(response.data.judul);
                            $('#id_news').val(response.data.id);
                            tinyMCE.get('isi').setContent(response.data.isi);
                            
                            if(response.data.status == "published"){
                                $("#status_published").prop("checked", true);
                            }else{
                                $("#status_unpublished").prop("checked", true);
                            }
                            
                            if(response.data.lampiran == null){
                                $('#btn-download-lampiran').hide();
                            }else{
                                $('#btn-download-lampiran').show();
                                $('#btn-download-lampiran').attr('href', response.data.lampiran_url);
                            }

                            $('#modal-form-news').modal({
                                backdrop: 'static',
                                keyboard: false
                            }, 'show');

                            $('.is-valid').removeClass('is-valid');
                            validator.resetForm();
                        } else {
                            showToast("danger", "Gagal!", "Gagal mengambil data");
                        }

                    }
                });
            });

            $(document).on('click', '.btn-delete-news', function() {
                var thisButton = $(this);

                var id_news = thisButton.data('idNews');

                if (confirm("Apakah Anda yakin akan menghapus data ini?")) {
                    $.ajax({
                        url: '{{ url('news/delete-news') }}',
                        data: {
                            id_news: id_news
                        },
                        method: 'GET',
                        beforeSend: function() {
                            thisButton.prop("disabled", true);
                            thisButton.html('<i class="fas fa-spinner fa-pulse"></i> Loading');
                        },
                        success: function(response) {
                            if (response.success) {
                                //reload ajax
                                table.ajax.reload();

                                //show toast
                                showToast("success", "Berhasil", response.message);
                            } else {
                                showToast("danger", "Gagal!", "Silakan coba lagi.");
                            }
                        }
                    });
                }
                return false;
            });
        });
    </script>
@endsection
