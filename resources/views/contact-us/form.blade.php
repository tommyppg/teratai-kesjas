<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>TERATAI KESJAS - Contact Us</title>

    <link rel="icon" type="image/x-icon" href="{{ asset('img/favicon.ico') }}">

    <!-- Custom fonts for this template-->
    <link href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="{{ asset('css/sb-admin-2.css') }}" rel="stylesheet">

</head>

<body class="bg-gradient-primary">

    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center">

            <div class="col-xl-10 col-lg-12 col-md-9">

                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
                            <div class="col-lg-6">
                                <div class="p-5">
                                    <div class="text-center">
                                        <img style="margin-bottom: 15px !important;" src="{{ asset('img/logo-brimob.png') }}" width="100px">
                                        <img style="margin-bottom: 15px !important;" src="{{ asset('img/logo-brimob-u-indonesia.png') }}" width="150px">
                                        <h1 class="h4 text-gray-900 mb-4"><strong>Contact Us</strong></h1>
                                        <p style="margin-top: -22px !important;">Formulir Kontak Kami</p>
                                    </div>
                                    <form action="{{ url('/contact-us/send') }}" id="sign-in" class="user">
                                        @csrf
                                        <div id="alert-login-success" class="alert alert-success d-none" role="alert">
                                            Pesan Terkirim
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control form-control-user" id="nama" name="nama" aria-describedby="nama" placeholder="Masukkan nama Anda">
                                        </div>
                                        <div class="form-group">
                                            <input type="email" class="form-control form-control-user" id="email" name="email" aria-describedby="email" placeholder="Masukkan email Anda">
                                        </div>
                                        <div class="form-group">
                                            <textarea class="form-control form-control-user" id="isi" name="isi" aria-describedby="isi" placeholder="Masukkan pesan Anda"></textarea>
                                        </div>
                                        <button type="submit" class="btn btn-primary btn-user btn-block" id="btn-submit">
                                            <i class="fas fa-paper-plane"></i> Kirim
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendor/jquery-validation-1.19.5/dist/jquery.validate.js'); }}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{ asset('js/sb-admin-2.min.js') }}"></script>

    <script>
        $( document ).ready( function () {
            $( "#sign-in" ).validate( {
				rules: {
					email: {
						required: true,
						email: true
					},
					nama: {
						required: true
					},
                    isi: {
						required: true
					},
				},
				messages: {
                    email: {
						required: "Mohon masukkan email Anda",
						email: "Format email tidak sesuai"
					},
                    nama: {
						required: "Mohon masukkan nama Anda"
					},
                    isi: {
						required: "Mohon masukkan isi pesan Anda"
					}
				},
				errorElement: "div",
                errorClass: 'invalid-feedback d-block',
				errorPlacement: function ( error, element ) {
					if ( element.prop( "type" ) === "checkbox" ) {
						error.insertAfter( element.parent( "label" ) );
					} else {
						error.insertAfter( element );
					}
				},
				highlight: function ( element, errorClass, validClass ) {
					$( element ).removeClass( "is-valid" );
				},
				unhighlight: function (element, errorClass, validClass) {
					$( element ).addClass( "is-valid" );
				},
                submitHandler: function (form) {
                    $.ajax({
                        type: "POST",
                        url: $('#sign-in').attr('action'),
                        data: $('#sign-in').serialize(),
                        beforeSend: function(){
                            //change button condition
                            $('#btn-submit').prop('disabled', true);
                            $('#btn-submit').html('Loading...');

                            //get rid alert
                            $('#alert-login-failed').addClass('d-none');
                            $('#alert-login-failed').removeClass('d-block');
                        },
                        success: function (result) {
                            // if(result.success){
                            //     window.location.href = "{{ url('/') }}";
                            // }else{
                            //     $('#alert-login-failed').removeClass('d-none');
                            //     $('#alert-login-failed').addClass('d-block');
                            // }

                            $('#alert-login-success').removeClass('d-none');
                            $('#alert-login-success').addClass('d-block');

                            $('#btn-submit').prop('disabled', false);
                            $('#btn-submit').html('Login');
                        },
                        fail: function(xhr, textStatus, errorThrown){
                            $('#alert-login-success').removeClass('d-none');
                            $('#alert-login-success').addClass('d-block');
                        },
                        error: function(xhr, textStatus, errorThrown){
                            $('#alert-login-success').removeClass('d-none');
                            $('#alert-login-success').addClass('d-block');
                        }
                    });
                    return false; // required to block normal submit since you used ajax
                }
			} );
        });
    </script>

</body>

</html>