@extends('layouts.main')

@section('css-page-spesific-plugin')
    <link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection

@section('js-page-spesific-plugin')
    <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendor/jquery-validation-1.19.5/dist/jquery.validate.js') }}"></script>
@endsection

@section('content')
    <h1 class="h3 mb-2 text-gray-800">Detail Monitoring Penyakit: {{ $kategoriPenyakit->nama_kategori_penyakit }}</h1>
    <p class="mb-4">Menampilkan data hasil rikkes yang memiliki penyakit tertentu</p>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Data Monitoring</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            {{-- <th>No</th> --}}
                            <th>Nama</th>
                            <th>NRP</th>
                            <th>Tahun Rikkes</th>
                            <th>Intensif</th>
                            <th>Nilai</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tfoot>

                        </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection

@section('js-page-custom')
    <script>
        // Call the dataTables jQuery plugin
        $(document).ready(function() {

            var table = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ url('penyakit/get-data-detail-monitoring/'.$kategoriPenyakit->id) }}",
                columns: [
                    // {
                    //     data: 'DT_RowIndex',
                    //     name: 'DT_RowIndex'
                    // },
                    {
                        data: 'nama',
                        name: 'nama',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'nrp',
                        name: 'nrp',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'tahun_rikkes',
                        name: 'tahun_rikkes',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'intensif',
                        name: 'intensif',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'kualitas',
                        name: 'kualitas',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'action',
                        name: 'action',
                    },
                ],
                "rowCallback": function(row, data) {
                    switch(data.kualitas){
                        case "Baik (B)":
                            $('td:eq(4)', row).html('<span class="badge bg-success text-white">'+data.kualitas+'</span>');
                            break;
                        case "Cukup (C)":
                            $('td:eq(4)', row).html('<span class="badge bg-info text-white">'+data.kualitas+'</span>');
                            break;
                        case "Kurang (K1)":
                            $('td:eq(4)', row).html('<span class="badge bg-warning text-white">'+data.kualitas+'</span>');
                            break;
                            case "Kurang Sekali (K2)":
                            $('td:eq(4)', row).html('<span class="badge bg-dark text-white">'+data.kualitas+'</span>');
                            break;
                        default:
                            break;
                    }
                }
            });
        });
    </script>
@endsection
