@extends('layouts.main')

@section('css-page-spesific-plugin')
    <link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection

@section('js-page-spesific-plugin')
    <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendor/jquery-validation-1.19.5/dist/jquery.validate.js') }}"></script>
@endsection

@section('content')
    <h1 class="h3 mb-2 text-gray-800">Manajemen Data Master: {{ $kategoriPenyakit->nama_kategori_penyakit }}</h1>
    <p class="mb-4">Data master untuk mengelola daftar kelainan yang berfungsi untuk mengidentifikasi hasil Rikkes. Mohon dapat mengisikan daftar ini sesuai dengan kata yang sama dengan yang ada di excel Rikkes.</p>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Data Daftar Kelainan</h6>
        </div>
        <div class="card-body">
            <button type="button" class="btn btn-primary float-end" id="btn-show-form-daftar-kelainan"><i
                class="fas fa-plus-circle"></i> Tambah Daftar</button>
            <br>
            <br>
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            {{-- <th>No</th> --}}
                            <th>Nama Kelainan</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- Form daftar-kelainan Modal-->
    <div class="modal fade" id="modal-form-daftar-kelainan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <form action="{{ url('penyakit/add-or-update-daftar-kelainan') }}" id="form-daftar-kelainan">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Tambah Daftar Kelainan</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="nama">Nama Daftar Kelainan</label>
                            <input type="text" class="form-control" name="nama_kelainan" id="nama_kelainan">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="id_kategori_penyakit" id="id_kategori_penyakit" value="{{ $kategoriPenyakit->id }}">
                        <input type="hidden" name="id_daftar_kelainan" id="id_daftar_kelainan" value="">
                        <input type="hidden" name="form_mode" id="form_mode" value="add">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary" id="btn-submit">Simpan</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('js-page-custom')
    <script>
        // Call the dataTables jQuery plugin
        $(document).ready(function() {

            var table = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ url('penyakit/get-data-master-daftar-kelainan/'.$kategoriPenyakit->id) }}",
                columns: [
                    // {
                    //     data: 'DT_RowIndex',
                    //     name: 'DT_RowIndex'
                    // },
                    {
                        data: 'nama_kelainan',
                        name: 'nama_kelainan',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'action',
                        name: 'action',
                    },
                ]
            });

            var validator = $("#form-daftar-kelainan").validate({
                rules: {
                    nama: {
                        required: true
                    }
                },
                messages: {
                    nama: {
                        required: "Mohon masukkan nama kelainan"
                    }
                },
                errorElement: "div",
                errorClass: 'invalid-feedback d-block',
                errorPlacement: function(error, element) {
                    if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.parent("label"));
                    } else {
                        error.insertAfter(element);
                    }
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).removeClass("is-valid");
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-valid");
                },
                submitHandler: function(form) {
                    $.ajax({
                        url: $('#form-daftar-kelainan').attr('action'),
                        data: $('#form-daftar-kelainan').serializeArray(),
                        method: 'POST',
                        beforeSend: function() {
                            $("#btn-submit").prop("disabled", true);
                            $("#btn-submit").html(
                                '<i class="fas fa-spinner fa-pulse"></i> Simpan');
                        },
                        success: function(response) {
                            //reset form
                            $("#btn-submit").prop("disabled", false);
                            $("#btn-submit").html('Simpan');

                            if (response.success) {
                                //hide modal
                                $('#modal-form-daftar-kelainan').modal('hide');

                                //reload ajax
                                table.ajax.reload();

                                //show toast
                                showToast("success", "Berhasil", response.message);
                            } else {
                                var messages = '';
                                $.each(response.message, function (index, value) {
                                    messages = messages + value + "<br>";
                                });

                                showToast("danger", "Gagal!", messages);
                            }
                        }
                    });
                    return false; // required to block normal submit since you used ajax
                }
            });

            $('#btn-show-form-daftar-kelainan').click(function() {
                $('#modal-form-daftar-kelainan').modal({
                    backdrop: 'static',
                    keyboard: false
                }, 'show');

                $('#form_mode').val('add');
                $('#nama_kelainan').val('');

                $('.is-valid').removeClass('is-valid');
                validator.resetForm();
            });

            $(document).on('click', '.btn-update-daftar-kelainan', function() {
                var thisButton = $(this);

                var id_daftar_kelainan = thisButton.data('idDaftarKelainan');

                $.ajax({
                    url: '{{ url('penyakit/get-daftar-kelainan') }}',
                    data: {
                        id_daftar_kelainan: id_daftar_kelainan
                    },
                    method: 'GET',
                    beforeSend: function() {
                        thisButton.prop("disabled", true);
                        thisButton.html('<i class="fas fa-spinner fa-pulse"></i> Loading');
                    },
                    success: function(response) {
                        thisButton.prop("disabled", false);
                        thisButton.html('<i class="fas fa-edit"></i> Update');
                        
                        if (response.data != null) {
                            $('#form_mode').val('update');
                            $('#nama_kelainan').val(response.data.nama_kelainan);
                            $('#id_daftar_kelainan').val(response.data.id);

                            $('#modal-form-daftar-kelainan').modal({
                                backdrop: 'static',
                                keyboard: false
                            }, 'show');
                        } else {
                            showToast("danger", "Gagal!", "Gagal mengambil data");
                        }

                    }
                });
            });

            $(document).on('click', '.btn-delete-daftar-kelainan', function() {
                var thisButton = $(this);

                var id_daftar_kelainan = thisButton.data('idDaftarKelainan');

                if (confirm("Apakah Anda yakin akan menghapus data ini?")) {
                    $.ajax({
                        url: '{{ url('penyakit/delete-daftar-kelainan') }}',
                        data: {
                            id_daftar_kelainan: id_daftar_kelainan
                        },
                        method: 'GET',
                        beforeSend: function() {
                            thisButton.prop("disabled", true);
                            thisButton.html('<i class="fas fa-spinner fa-pulse"></i> Loading');
                        },
                        success: function(response) {
                            if (response.success) {
                                //reload ajax
                                table.ajax.reload();

                                //show toast
                                showToast("success", "Berhasil", response.message);
                            } else {
                                showToast("danger", "Gagal!", "Silakan coba lagi.");
                            }
                        }
                    });
                }
                return false;
            });
        });
    </script>
@endsection
