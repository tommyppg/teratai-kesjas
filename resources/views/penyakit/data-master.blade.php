@extends('layouts.main')

@section('css-page-spesific-plugin')
    <link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection

@section('js-page-spesific-plugin')
    <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendor/jquery-validation-1.19.5/dist/jquery.validate.js') }}"></script>
@endsection

@section('content')
    <h1 class="h3 mb-2 text-gray-800">Manajemen Data Master</h1>
    <p class="mb-4">Data master untuk mengelola data penyakit dan kelainan.</p>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Data Kategori Penyakit</h6>
        </div>
        <div class="card-body">
            <button type="button" class="btn btn-primary float-end" id="btn-show-form-kategori-penyakit"><i
                class="fas fa-plus-circle"></i> Tambah Kategori</button>
            <br>
            <br>
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            {{-- <th>No</th> --}}
                            <th>Nama Kategori Penyakit</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- Form kategori-penyakit Modal-->
    <div class="modal fade" id="modal-form-kategori-penyakit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <form action="{{ url('penyakit/add-or-update-kategori-penyakit') }}" id="form-kategori-penyakit">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Tambah Kategori Penyakit</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="nama">Nama Kategori Penyakit</label>
                            <input type="text" class="form-control" name="nama_kategori_penyakit" id="nama_kategori_penyakit">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="id_kategori_penyakit" id="id_kategori_penyakit" value="">
                        <input type="hidden" name="form_mode" id="form_mode" value="add">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary" id="btn-submit">Simpan</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('js-page-custom')
    <script>
        // Call the dataTables jQuery plugin
        $(document).ready(function() {

            var table = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ url('penyakit/get-data-master-kategori-penyakit') }}",
                columns: [
                    // {
                    //     data: 'DT_RowIndex',
                    //     name: 'DT_RowIndex'
                    // },
                    {
                        data: 'nama_kategori_penyakit',
                        name: 'nama_kategori_penyakit',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'action',
                        name: 'action',
                    },
                ]
            });

            var validator = $("#form-kategori-penyakit").validate({
                rules: {
                    nama: {
                        required: true
                    }
                },
                messages: {
                    nama: {
                        required: "Mohon masukkan nama kategori penyakit"
                    }
                },
                errorElement: "div",
                errorClass: 'invalid-feedback d-block',
                errorPlacement: function(error, element) {
                    if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.parent("label"));
                    } else {
                        error.insertAfter(element);
                    }
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).removeClass("is-valid");
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-valid");
                },
                submitHandler: function(form) {
                    $.ajax({
                        url: $('#form-kategori-penyakit').attr('action'),
                        data: $('#form-kategori-penyakit').serializeArray(),
                        method: 'POST',
                        beforeSend: function() {
                            $("#btn-submit").prop("disabled", true);
                            $("#btn-submit").html(
                                '<i class="fas fa-spinner fa-pulse"></i> Simpan');
                        },
                        success: function(response) {
                            //reset form
                            $("#btn-submit").prop("disabled", false);
                            $("#btn-submit").html('Simpan');

                            if (response.success) {
                                //hide modal
                                $('#modal-form-kategori-penyakit').modal('hide');

                                //reload ajax
                                table.ajax.reload();

                                //show toast
                                showToast("success", "Berhasil", response.message);
                            } else {
                                var messages = '';
                                $.each(response.message, function (index, value) {
                                    messages = messages + value + "<br>";
                                });

                                showToast("danger", "Gagal!", messages);
                            }
                        }
                    });
                    return false; // required to block normal submit since you used ajax
                }
            });

            $('#btn-show-form-kategori-penyakit').click(function() {
                $('#modal-form-kategori-penyakit').modal({
                    backdrop: 'static',
                    keyboard: false
                }, 'show');

                $('#form_mode').val('add');
                $('#nama_kategori_penyakit').val('');

                $('.is-valid').removeClass('is-valid');
                validator.resetForm();
            });

            $(document).on('click', '.btn-update-kategori-penyakit', function() {
                var thisButton = $(this);

                var id_kategori_penyakit = thisButton.data('idKategoriPenyakit');

                $.ajax({
                    url: '{{ url('penyakit/get-kategori-penyakit') }}',
                    data: {
                        id_kategori_penyakit: id_kategori_penyakit
                    },
                    method: 'GET',
                    beforeSend: function() {
                        thisButton.prop("disabled", true);
                        thisButton.html('<i class="fas fa-spinner fa-pulse"></i> Loading');
                    },
                    success: function(response) {
                        thisButton.prop("disabled", false);
                        thisButton.html('<i class="fas fa-edit"></i> Update');
                        
                        if (response.data != null) {
                            $('#form_mode').val('update');
                            $('#nama_kategori_penyakit').val(response.data.nama_kategori_penyakit);
                            $('#id_kategori_penyakit').val(response.data.id);

                            $('#modal-form-kategori-penyakit').modal({
                                backdrop: 'static',
                                keyboard: false
                            }, 'show');
                        } else {
                            showToast("danger", "Gagal!", "Gagal mengambil data");
                        }

                    }
                });
            });

            $(document).on('click', '.btn-delete-kategori-penyakit', function() {
                var thisButton = $(this);

                var id_kategori_penyakit = thisButton.data('idKategoriPenyakit');

                if (confirm("Apakah Anda yakin akan menghapus data ini?")) {
                    $.ajax({
                        url: '{{ url('penyakit/delete-kategori-penyakit') }}',
                        data: {
                            id_kategori_penyakit: id_kategori_penyakit
                        },
                        method: 'GET',
                        beforeSend: function() {
                            thisButton.prop("disabled", true);
                            thisButton.html('<i class="fas fa-spinner fa-pulse"></i> Loading');
                        },
                        success: function(response) {
                            if (response.success) {
                                //reload ajax
                                table.ajax.reload();

                                //show toast
                                showToast("success", "Berhasil", response.message);
                            } else {
                                showToast("danger", "Gagal!", "Silakan coba lagi.");
                            }
                        }
                    });
                }
                return false;
            });
        });
    </script>
@endsection
