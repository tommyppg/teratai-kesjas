@extends('layouts.main')

@section('css-page-spesific-plugin')
    <link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection

@section('js-page-spesific-plugin')
    <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendor/jquery-validation-1.19.5/dist/jquery.validate.js') }}"></script>
@endsection

@section('content')
    <h1 class="h3 mb-2 text-gray-800">Detail Faskes</h1>
    <p class="mb-4">Tambahkan akun faskes di sini.</p>

    <div class="row">
        <div class="col-md-12">
            <!-- DataTales Example -->
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Data Akun {{ $dataFaskes->nama_satker }}</h6>
                </div>
                <div class="card-body">
                    <button type="button" class="btn btn-primary float-end" id="btn-show-form-user"><i
                            class="fas fa-plus-circle"></i> Tambah Akun</button>
                    <br>
                    <br>
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    {{-- <th>No</th> --}}
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        @if($dataFaskes->tipe == "rs")
        <div class="col-md-12">
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Data Unit Poli</h6>
            </div>
            <div class="card-body">
                <button type="button" class="btn btn-primary float-end" id="btn-show-form-poli"><i
                    class="fas fa-plus-circle"></i> Tambah Unit Poli</button>
                <br>
                <br>
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable2" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                {{-- <th>No</th> --}}
                                <th>Nama Unit Poli</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        </div>
        @endif
    </div>


    <!-- Form User Modal-->
    <div class="modal fade" id="modal-form-user" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <form action="{{ url('faskes/add-or-update-user') }}" id="form-user">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Tambah Akun</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="nama">Nama</label>
                            <input type="text" class="form-control" name="nama" id="nama">
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" name="email" id="email">
                        </div>
                        <div class="form-check" id="ubah-password-check">
                            <input class="form-check-input" name="ubah_password" type="checkbox" value="ubah_password" id="ubah-password-checkbox">
                            <label class="form-check-label" for="ubah-password-checkbox">
                                Ubah password?
                            </label>
                          </div>
                        <div class="form-group" id="password-field">
                            <label for="email">Password</label>
                            <input type="password" class="form-control" name="password" id="password">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="id_satker" id="id_satker" value="{{ $dataFaskes->id }}">
                        <input type="hidden" name="id_user" id="id_user" value="">
                        <input type="hidden" name="role" id="role" value="{{ $dataFaskes->tipe }}">
                        <input type="hidden" name="form_mode" id="form_mode" value="add">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary" id="btn-submit">Simpan</a>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <!-- Form Faskes Modal-->
    @if($dataFaskes->tipe == "rs")
    <div class="modal fade" id="modal-form-poli" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <form action="{{ url('faskes/add-or-update-poli') }}" id="form-poli">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Tambah Poli</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="nama">Nama Faskes</label>
                            <input type="text" class="form-control" name="nama_faskes" id="nama_faskes" value="{{ $dataFaskes->nama_satker }}" disabled>
                        </div>
                        <div class="form-group">
                            <label for="nama_poli">Nama Poli</label>
                            <input type="text" class="form-control" name="nama_poli" id="nama_poli">
                        </div>
                        <div class="form-group">
                            <label for="jenis_poli">Jenis Poli</label>
                            <select name="jenis_poli" id="jenis_poli" class="form-control">
                                <option value="">--PILIH TIPE POLI--</option>
                                @foreach($jenisPoli as $poli)
                                <option value="{{ $poli->id }}">{{ $poli->nama_jenis_poli }}</option>
                                @endforeach
                            </select>
                            <small><em>Data ini dapat ditambahkan pada menu Manajemen Data Master Poli pada <a target="_blank" href="{{ url('faskes/data-master-poli') }}">halaman berikut</a> kemudian refresh kembali.</em></small>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="id_faskes" id="id_faskes" value="">
                        <input type="hidden" name="form_mode" id="form_mode_poli" value="add">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary" id="btn-submit">Simpan</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
    @endif
@endsection

@section('js-page-custom')
    <script>
        // Call the dataTables jQuery plugin
        $(document).ready(function() {
            var table = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ url('faskes/get-data-user/' . $dataFaskes->id) }}",
                columns: [
                    // {
                    //     data: 'DT_RowIndex',
                    //     name: 'DT_RowIndex'
                    // },
                    {
                        data: 'nama',
                        name: 'nama',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'email',
                        name: 'email',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'action',
                        name: 'action',
                    },
                ]
            });

            var validator = $("#form-user").validate({
                rules: {
                    nama: {
                        required: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true
                    },
                },
                messages: {
                    nama: {
                        required: "Mohon masukkan nama"
                    },
                    email: {
                        required: "Mohon masukkan email",
                        email: "Format email tidak sesuai"
                    },
                    password: {
                        required: "Mohon masukkan password"
                    },
                },
                errorElement: "div",
                errorClass: 'invalid-feedback d-block',
                errorPlacement: function(error, element) {
                    if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.parent("label"));
                    } else {
                        error.insertAfter(element);
                    }
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).removeClass("is-valid");
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-valid");
                },
                submitHandler: function(form) {
                    $.ajax({
                        url: $('#form-user').attr('action'),
                        data: $('#form-user').serializeArray(),
                        method: 'POST',
                        beforeSend: function() {
                            $("#btn-submit").prop("disabled", true);
                            $("#btn-submit").html(
                                '<i class="fas fa-spinner fa-pulse"></i> Simpan');
                        },
                        success: function(response) {
                            //reset form
                            $("#btn-submit").prop("disabled", false);
                            $("#btn-submit").html('Simpan');

                            if (response.success) {
                                //hide modal
                                $('#modal-form-user').modal('hide');

                                //reload ajax
                                table.ajax.reload();

                                //show toast
                                showToast("success", "Berhasil", response.message);
                            } else {
                                if(response.message.prop && response.message.prop.constructor === Array){
                                    var messages = '';
                                    $.each(response.message, function (index, value) {
                                        messages = messages + value + "<br>";
                                    });

                                    showToast("danger", "Gagal!", messages);
                                }else{
                                    showToast("danger", "Gagal!", response.message);
                                }
                            }
                        }
                    });
                    return false; // required to block normal submit since you used ajax
                }
            });

            $('#btn-show-form-user').click(function() {
                $('#modal-form-user').modal({
                    backdrop: 'static',
                    keyboard: false
                }, 'show');

                $('#form_mode').val('add');
                $('#nama').val('');
                $('#email').val('');
                $('#password').val('');
                $('#password-field').show();

                $('#ubah-password-checkbox').prop('checked', false);
                $('#ubah-password-check').hide();

                $('.is-valid').removeClass('is-valid');
                validator.resetForm();
            });

            $(document).on('click', '.btn-update-user', function() {
                var thisButton = $(this);

                var id_user = thisButton.data('idUser');

                $.ajax({
                    url: '{{ url('faskes/get-user') }}',
                    data: {
                        id_user: id_user
                    },
                    method: 'GET',
                    beforeSend: function() {
                        thisButton.prop("disabled", true);
                        thisButton.html('<i class="fas fa-spinner fa-pulse"></i> Loading');
                    },
                    success: function(response) {
                        thisButton.prop("disabled", false);
                        thisButton.html('<i class="fas fa-edit"></i> Update');

                        if (response.data != null) {
                            $('#form_mode').val('update');
                            $('#nama').val(response.data.nama);
                            $('#email').val(response.data.email);
                            $('#id_user').val(response.data.id);
                            $('#id_satker').val(response.data.id_satker);
                            $('#password-field').hide();

                            $('#ubah-password-checkbox').prop('checked', false);
                            $('#ubah-password-check').show();

                            $('.is-valid').removeClass('is-valid');

                            $('#modal-form-user').modal({
                                backdrop: 'static',
                                keyboard: false
                            }, 'show');
                        } else {

                        }

                    }
                });
            });

            $("#ubah-password-checkbox").change(function() {
                $('#password').val('');

                if(this.checked) {
                    $('#password-field').show();
                }else{
                    $('#password-field').hide();
                }
            });

            $(document).on('click', '.btn-delete-user', function() {
                var thisButton = $(this);

                var id_user = thisButton.data('idUser');

                if (confirm("Apakah Anda yakin akan menghapus data ini?")) {
                    $.ajax({
                        url: '{{ url('faskes/delete-user') }}',
                        data: {
                            id_user: id_user
                        },
                        method: 'GET',
                        beforeSend: function() {
                            thisButton.prop("disabled", true);
                            thisButton.html('<i class="fas fa-spinner fa-pulse"></i> Loading');
                        },
                        success: function(response) {
                            if (response.success) {
                                //reload ajax
                                table.ajax.reload();

                                //show toast
                                showToast("success", "Berhasil", response.message);
                            } else {
                                showToast("danger", "Gagal!", "Silakan coba lagi.");
                            }
                        }
                    });
                }
                return false;
            });
        });

        @if($dataFaskes->tipe == "rs")
        var table = $('#dataTable2').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ url('faskes/get-data-faskes-poli/'.$dataFaskes->id) }}",
                columns: [
                    // {
                    //     data: 'DT_RowIndex',
                    //     name: 'DT_RowIndex'
                    // },
                    {
                        data: 'nama_jenis_poli',
                        name: 'nama_jenis_poli',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'action',
                        name: 'action',
                    },
                ]
            });

            $('#btn-show-form-poli').click(function() {
                $('#modal-form-poli').modal({
                    backdrop: 'static',
                    keyboard: false
                }, 'show');

                $('#form_mode_poli').val('add');
                $('#nama_poli').val('');
                $('#jenis_poli').val('');

                $('.is-valid').removeClass('is-valid');
                validator.resetForm();
            });
        @endif
    </script>
@endsection
