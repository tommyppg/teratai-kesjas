@extends('layouts.main')

@section('css-page-spesific-plugin')
    <link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection

@section('js-page-spesific-plugin')
    <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendor/jquery-validation-1.19.5/dist/jquery.validate.js') }}"></script>
@endsection

@section('content')
    <h1 class="h3 mb-2 text-gray-800">Manajemen Data Master Poli</h1>
    <p class="mb-4">Manajemen data master untuk jenis poli di fasilitas kesehatan</p>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Data Jenis Poli</h6>
        </div>
        <div class="card-body">
            <button type="button" class="btn btn-primary float-end" id="btn-show-form-jenis-poli"><i
                class="fas fa-plus-circle"></i> Tambah Data Master</button>
            <br>
            <br>
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            {{-- <th>No</th> --}}
                            <th>Nama Poli</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tfoot>

                        </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- Form Faskes Modal-->
    <div class="modal fade" id="modal-form-jenis-poli" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <form action="{{ url('faskes/add-or-update-jenis-poli') }}" id="form-jenis-poli">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Tambah Jenis Poli</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="nama">Nama Jenis Poli</label>
                            <input type="text" class="form-control" name="nama_jenis_poli" id="nama_jenis_poli">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="id_jenis_poli" id="id_jenis_poli" value="">
                        <input type="hidden" name="form_mode" id="form_mode" value="add">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary" id="btn-submit">Simpan</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('js-page-custom')
    <script>
        // Call the dataTables jQuery plugin
        $(document).ready(function() {

            var table = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ url('faskes/get-data-master-jenis-poli') }}",
                columns: [
                    // {
                    //     data: 'DT_RowIndex',
                    //     name: 'DT_RowIndex'
                    // },
                    {
                        data: 'nama_jenis_poli',
                        name: 'nama_jenis_poli',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'action',
                        name: 'action',
                    },
                ]
            });

            var validator = $("#form-jenis-poli").validate({
                rules: {
                    nama_jenis_poli: {
                        required: true
                    }
                },
                messages: {
                    nama: {
                        required: "Mohon masukkan nama"
                    }
                },
                errorElement: "div",
                errorClass: 'invalid-feedback d-block',
                errorPlacement: function(error, element) {
                    if (element.prop("type") === "checkbox") {
                        error.insertAfter(element.parent("label"));
                    } else {
                        error.insertAfter(element);
                    }
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).removeClass("is-valid");
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).addClass("is-valid");
                },
                submitHandler: function(form) {
                    $.ajax({
                        url: $('#form-jenis-poli').attr('action'),
                        data: $('#form-jenis-poli').serializeArray(),
                        method: 'POST',
                        beforeSend: function() {
                            $("#btn-submit").prop("disabled", true);
                            $("#btn-submit").html(
                                '<i class="fas fa-spinner fa-pulse"></i> Simpan');
                        },
                        success: function(response) {
                            //reset form
                            $("#btn-submit").prop("disabled", false);
                            $("#btn-submit").html('Simpan');

                            if (response.success) {
                                //hide modal
                                $('#modal-form-jenis-poli').modal('hide');

                                //reload ajax
                                table.ajax.reload();

                                //show toast
                                showToast("success", "Berhasil", response.message);
                            } else {
                                if(response.message.prop && response.message.prop.constructor === Array){
                                    var messages = '';
                                    $.each(response.message, function (index, value) {
                                        messages = messages + value + "<br>";
                                    });

                                    showToast("danger", "Gagal!", messages);
                                }else{
                                    showToast("danger", "Gagal!", response.message);
                                }
                            }
                        }
                    });
                    return false; // required to block normal submit since you used ajax
                }
            });

            $('#btn-show-form-jenis-poli').click(function() {
                $('#modal-form-jenis-poli').modal({
                    backdrop: 'static',
                    keyboard: false
                }, 'show');

                $('#form_mode').val('add');
                $('#nama_jenis_poli').val('');

                $('.is-valid').removeClass('is-valid');
                validator.resetForm();
            });

            $(document).on('click', '.btn-update-jenis-poli', function() {
                var thisButton = $(this);

                var id_jenis_poli = thisButton.data('idJenisPoli');

                $.ajax({
                    url: '{{ url('faskes/get-jenis-poli') }}',
                    data: {
                        id_jenis_poli: id_jenis_poli
                    },
                    method: 'GET',
                    beforeSend: function() {
                        thisButton.prop("disabled", true);
                        thisButton.html('<i class="fas fa-spinner fa-pulse"></i> Loading');
                    },
                    success: function(response) {
                        thisButton.prop("disabled", false);
                        thisButton.html('<i class="fas fa-edit"></i> Update');
                        
                        if (response.data != null) {
                            $('#form_mode').val('update');
                            $('#nama_jenis_poli').val(response.data.nama_jenis_poli);
                            $('#id_jenis_poli').val(response.data.id);

                            $('#modal-form-jenis-poli').modal({
                                backdrop: 'static',
                                keyboard: false
                            }, 'show');
                        } else {
                            showToast("danger", "Gagal!", "Gagal mengambil data");
                        }

                    }
                });
            });

            $(document).on('click', '.btn-delete-jenis-poli', function() {
                var thisButton = $(this);

                var id_jenis_poli = thisButton.data('idJenisPoli');

                if (confirm("Apakah Anda yakin akan menghapus data ini?")) {
                    $.ajax({
                        url: '{{ url('faskes/delete-jenis-poli') }}',
                        data: {
                            id_jenis_poli: id_jenis_poli
                        },
                        method: 'GET',
                        beforeSend: function() {
                            thisButton.prop("disabled", true);
                            thisButton.html('<i class="fas fa-spinner fa-pulse"></i> Loading');
                        },
                        success: function(response) {
                            if (response.success) {
                                //reload ajax
                                table.ajax.reload();

                                //show toast
                                showToast("success", "Berhasil", response.message);
                            } else {
                                showToast("danger", "Gagal!", "Silakan coba lagi.");
                            }
                        }
                    });
                }
                return false;
            });
        });
    </script>
@endsection
