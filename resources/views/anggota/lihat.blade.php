@extends('layouts.main')

@section('css-page-spesific-plugin')
    <link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection

@section('js-page-spesific-plugin')
    <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
@endsection

@section('content')
    <h1 class="h3 mb-2 text-gray-800">Lihat Data Anggota</h1>
    <p class="mb-4">Data anggota berasal dari hasil import excel Rikkes</p>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Data Anggota</h6>
        </div>
        <div class="card-body">
            {{-- <h6 class="m-0 font-weight-bold text-primary">Filter Data</h6> --}}
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="tahun">Tahun Rikkes/TKJ</label>
                            <select name="tahun" id="tahun" class="form-control">
                                <option value="">--Pilih Filter--</option>
                                @for ($i = date('Y') + 2; $i >= date('Y') - 2; $i--)
                                    <option value="{{ $i }}" {{ date('Y') == $i ? 'selected' : '' }}>
                                        {{ $i }}</option>
                                @endfor
                            </select>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="status_rikkes">Status Rikkes</label>
                            <select name="status_rikkes" id="status_rikkes" class="form-control">
                                <option value="">--Tanpa Filter--</option>
                                <option value="sudah_periksa">Sudah Periksa</option>
                                <option value="belum_periksa">Belum Periksa</option>
                                <option value="belum_rikkes">Belum Rikkes</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="status_tkj">Status TKJ</label>
                            <select name="status_tkj" id="status_tkj" class="form-control">
                                <option value="">--Tanpa Filter--</option>
                                <option value="sudah_tkj">Sudah TKJ</option>
                                <option value="belum_tkj">Belum TKJ</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <button type="button" id="btn-filter" class="btn btn-primary"><i class="fas fa-filter"></i> Filter</button>
                    </div>
                </div>
            <hr>
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            {{-- <th>No</th> --}}
                            <th>Nama</th>
                            <th>NRP</th>
                            <th>Pangkat</th>
                            <th>Satker</th>
                            <th>Status Anggota</th>
                            <th>Status Rikkes</th>
                            <th>Status TKJ</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tfoot>

                        </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('js-page-custom')
    <script>
        // Call the dataTables jQuery plugin
        $(document).ready(function() {

            var table = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{ url('anggota/get-data') }}",
                    data: function(d){
                        d.tahun = $('#tahun').val();
                        d.status_rikkes = $('#status_rikkes').val();
                        d.status_tkj = $('#status_tkj').val();
                    }
                },
                columns: [
                    // {
                    //     data: 'DT_RowIndex',
                    //     name: 'DT_RowIndex'
                    // },
                    {
                        data: 'nama',
                        name: 'nama',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'nrp',
                        name: 'nrp',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'pangkat',
                        name: 'pangkat',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'satker',
                        name: 'satker',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'status',
                        name: 'status',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'rikkes_status',
                        name: 'rikkes.status as rikkes_status',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'tkj_status',
                        name: 'tkj.nilai_akhir as tkj_status',
                        orderable: true,
                        searchable: true
                    },
                    {
                        data: 'action',
                        name: 'action',
                    },
                ]
            });

            $(document).on('click', '.btn-status-change', function() {
                var thisButton = $(this);

                var nama = $(this).data('namaUser');
                var current_status = $(this).data('currentStatus');
                var nrp = $(this).data('nrp');

                switch (current_status) {
                    case 'active':
                        var question = "Apakah Anda yakin akan menonaktifkan anggota: " + nama + "?";
                        break;
                    case 'inactive':
                        var question = "Apakah Anda yakin akan mengaktifkan kembali anggota: " + nama + "?";
                        break;
                    default:
                        break;
                }

                if (confirm(question)) {
                    $.ajax({
                        url: '{{ url('anggota/change-status-user') }}',
                        data: {
                            nrp: nrp,
                            current_status: current_status
                        },
                        method: 'GET',
                        beforeSend: function() {
                            thisButton.prop("disabled", true);
                            thisButton.html('<i class="fas fa-spinner fa-pulse"></i> Loading');
                        },
                        success: function(response) {
                            if (response.success) {
                                //reload ajax
                                table.ajax.reload();

                                switch (current_status) {
                                    case 'active':
                                        //show toast
                                        showToast("success", "Berhasil", "Berhasil menonaktifkan Anggota: "+nama);
                                        break;
                                    case 'inactive':
                                        showToast("success", "Berhasil", "Berhasil mengaktifkan kembali Anggota: "+nama);
                                        break;
                                    default:
                                        break;
                                }
                            } else {
                                showToast("danger", "Gagal!", "Silakan coba lagi.");
                            }
                        }
                    });
                }
                return false;
            });

            $('#btn-filter').click(function(){
                // $('#dataTable').dataTable().fnClearTable();
                table.clear().draw();
            });
        });
    </script>
@endsection
