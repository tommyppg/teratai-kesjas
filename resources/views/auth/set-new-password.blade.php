<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>TERATAI KESJAS - Set New Password</title>

    <link rel="icon" type="image/x-icon" href="{{ asset('img/favicon.ico') }}">

    <!-- Custom fonts for this template-->
    <link href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="{{ asset('css/sb-admin-2.css') }}" rel="stylesheet">

</head>

<body class="bg-gradient-primary">

    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center">

            <div class="col-xl-10 col-lg-12 col-md-9">

                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
                            <div class="col-lg-6">
                                <div class="p-5">
                                    <div class="text-center">
                                        <img style="margin-bottom: 15px !important;" src="{{ asset('img/logo-brimob.png') }}" width="150px">
                                        <h1 class="h4 text-gray-900 mb-4"><strong>Set Password Baru</strong></h1>
                                        <p style="margin-top: -22px !important;">Teknologi Informasi Kesehatan Terintegrasi</p>
                                    </div>
                                    <div id="alert-success-reset" class="alert alert-success d-none" role="alert">
                                        Password berhasil diganti. Anda dapat login kembali di aplikasi Teratai Kesjas menggunakan password baru.
                                    </div>
                                    <form action="{{ url('/set-new-password') }}" id="set-new-password-form" class="user">
                                        @csrf
                                        <div id="alert-login-failed" class="alert alert-danger d-none" role="alert">
                                            Username dan password tidak ditemukan!
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control form-control-user" id="password" name="password" placeholder="Password baru">
                                        </div>
                                        <div class="form-group">
                                            <input type="cpassword" class="form-control form-control-user" id="confirm_password" name="confirm_password" placeholder="Konfirmasi password baru">
                                        </div>
                                        <input type="hidden" name="token" value="{{ $token }}">
                                        <button type="submit" class="btn btn-primary btn-user btn-block" id="btn-submit">
                                            Simpan
                                        </button>
                                    </form>
                                    {{-- <hr>
                                    <div class="text-center">
                                        Lupa password? Klik <a href="forgot-password.html">di sini!</a>
                                    </div> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendor/jquery-validation-1.19.5/dist/jquery.validate.js'); }}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{ asset('js/sb-admin-2.min.js') }}"></script>

    <script>
        $( document ).ready( function () {
            $( "#set-new-password-form" ).validate( {
				rules: {
					password: {
						required: true
					},
                    confirm_password: {
						required: true,
                        equalTo : "#password"
					},
				},
				messages: {
					password: {
						required: "Mohon masukkan password Anda"
					},
                    confirm_password: {
						required: "Mohon masukkan password Anda",
                        equalTo: "Konfirmasi password harus sama"
					},
				},
				errorElement: "div",
                errorClass: 'invalid-feedback d-block',
				errorPlacement: function ( error, element ) {
					if ( element.prop( "type" ) === "checkbox" ) {
						error.insertAfter( element.parent( "label" ) );
					} else {
						error.insertAfter( element );
					}
				},
				highlight: function ( element, errorClass, validClass ) {
					$( element ).removeClass( "is-valid" );
				},
				unhighlight: function (element, errorClass, validClass) {
					$( element ).addClass( "is-valid" );
				},
                submitHandler: function (form) {
                    $.ajax({
                        type: "POST",
                        url: $('#set-new-password-form').attr('action'),
                        data: $('#set-new-password-form').serialize(),
                        beforeSend: function(){
                            //change button condition
                            $('#btn-submit').prop('disabled', true);
                            $('#btn-submit').html('Loading...');

                            //get rid alert
                            $('#alert-login-failed').addClass('d-none');
                            $('#alert-login-failed').removeClass('d-block');
                        },
                        success: function (result) {
                            if(result.success){
                                $('#alert-success-reset').removeClass('d-none');
                                $('#alert-success-reset').addClass('d-block');
                                $('#set-new-password-form').hide();
                            }else{
                                $('#alert-login-failed').removeClass('d-none');
                                $('#alert-login-failed').addClass('d-block');
                                $('#alert-login-failed').html(result.message);
                            }

                            $('#btn-submit').prop('disabled', false);
                            $('#btn-submit').html('Login');
                        }
                    });
                    return false; // required to block normal submit since you used ajax
                }
			} );
        });
    </script>

</body>

</html>