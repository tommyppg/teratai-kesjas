<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>TERATAI KESJAS</title>

    <link rel="icon" type="image/x-icon" href="{{ asset('img/favicon.ico') }}">

    <!-- Custom fonts for this template-->
    <link href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="{{ asset('css/sb-admin-2.css') }}" rel="stylesheet">

    <!-- Page spesific css -->
    @yield('css-page-spesific-plugin')

    <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
                <div class="sidebar-brand-icon rotate-n-15">
                    <i class="fas fa-medkit"></i>
                </div>
                <div class="sidebar-brand-text mx-3" style="font-size: 0.8rem !important;">TERATAI KESJAS</div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item {{ $dataNav['level1'] == "dashboard" ? "active" : "" }}">
                <a class="nav-link" href="{{ url(''); }}">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span></a>
            </li>

            {{-- <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Heading -->
            <div class="sidebar-heading">
                Interface
            </div> --}}

            <!-- Nav Item - Pages Collapse Menu -->
            @if(Auth::user()->role == "superadmin")
            <li class="nav-item {{ $dataNav['level1'] == "rikkes" ? "active" : "" }}">
                <a class="nav-link {{ $dataNav['level1'] == "rikkes" ? "" : "collapsed" }}" href="#" data-toggle="collapse" data-target="#collapseTwo"
                    aria-expanded="true" aria-controls="collapseTwo">
                    <i class="fas fa-archive"></i>
                    <span>Data Rikkes</span>
                </a>
                <div id="collapseTwo" class="collapse {{ $dataNav['level1'] == "rikkes" ? "show" : "" }}" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        {{-- <h6 class="collapse-header">Custom Components:</h6> --}}
                        <a class="collapse-item {{ $dataNav['level2'] == "rikkes.import" ? "active" : "" }}" href="{{ url('rikkes/import'); }}">Import Rikkes</a>
                        <a class="collapse-item {{ $dataNav['level2'] == "rikkes.lihat" ? "active" : "" }}" href="{{ url('rikkes/lihat'); }}">Lihat Rikkes</a>
                        <a class="collapse-item {{ $dataNav['level2'] == "rikkes.sudah-periksa" ? "active" : "" }}" href="{{ url('rikkes/sudah-periksa'); }}">Sudah Periksa</a>
                    </div>
                </div>
            </li>
            @endif

            <!-- Nav Item - Pages Collapse Menu -->
            @if(Auth::user()->role == "superadmin")
            <li class="nav-item {{ $dataNav['level1'] == "tkj" ? "active" : "" }}">
                <a class="nav-link {{ $dataNav['level1'] == "tkj" ? "" : "collapsed" }}" href="#" data-toggle="collapse" data-target="#collapseTKJ"
                    aria-expanded="true" aria-controls="collapseTKJ">
                    <i class="fas fa-calendar-check"></i>
                    <span>Data TKJ</span>
                </a>
                <div id="collapseTKJ" class="collapse {{ $dataNav['level1'] == "tkj" ? "show" : "" }}" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        {{-- <h6 class="collapse-header">Custom Components:</h6> --}}
                        <a class="collapse-item {{ $dataNav['level2'] == "tkj.import" ? "active" : "" }}" href="{{ url('tkj/import'); }}">Import Nilai TKJ</a>
                        <a class="collapse-item {{ $dataNav['level2'] == "tkj.lihat" ? "active" : "" }}" href="{{ url('tkj/lihat'); }}">Lihat Nilai TKJ</a>
                    </div>
                </div>
            </li>
            @endif

            <!-- Nav Item - Pages Collapse Menu -->
            @if(Auth::user()->role == "superadmin")
            <li class="nav-item {{ $dataNav['level1'] == "penyakit" ? "active" : "" }}">
                <a class="nav-link {{ $dataNav['level1'] == "penyakit" ? "" : "collapsed" }}" href="#" data-toggle="collapse" data-target="#collapsePenyakit"
                    aria-expanded="true" aria-controls="collapsePenyakit">
                    <i class="fas fa-viruses"></i>
                    <span>Data Penyakit</span>
                </a>
                <div id="collapsePenyakit" class="collapse {{ $dataNav['level1'] == "penyakit" ? "show" : "" }}" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        {{-- <h6 class="collapse-header">Custom Components:</h6> --}}
                        <a class="collapse-item {{ $dataNav['level2'] == "penyakit.lihat" ? "active" : "" }}" href="{{ url('penyakit/lihat'); }}">Monitoring Penyakit</a>
                        <a class="collapse-item {{ $dataNav['level2'] == "penyakit.data-master" ? "active" : "" }}" href="{{ url('penyakit/data-master'); }}">Data Master Penyakit</a>
                    </div>
                </div>
            </li>
            @endif

            <!-- Nav Item - Utilities Collapse Menu -->
            @if(Auth::user()->role == "superadmin")
            <li class="nav-item {{ $dataNav['level1'] == "anggota" ? "active" : "" }}">
                <a class="nav-link {{ $dataNav['level1'] == "anggota" ? "" : "collapsed" }}" href="#" data-toggle="collapse" data-target="#collapseUtilities"
                    aria-expanded="true" aria-controls="collapseUtilities">
                    <i class="fas fa-users"></i>
                    <span>Data Anggota</span>
                </a>
                <div id="collapseUtilities" class="collapse {{ $dataNav['level1'] == "anggota" ? "show" : "" }}" aria-labelledby="headingUtilities"
                    data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <a class="collapse-item {{ $dataNav['level2'] == "anggota.lihat" ? "active" : "" }}" href="{{ url('anggota/lihat'); }}">Lihat Anggota</a>
                    </div>
                </div>
            </li>
            @endif

            <!-- Nav Item - Utilities Collapse Menu -->
            @if(Auth::user()->role == "superadmin")
            <li class="nav-item {{ $dataNav['level1'] == "faskes" ? "active" : "" }}">
                <a class="nav-link {{ $dataNav['level1'] == "faskes" ? "" : "collapsed" }}" href="#" data-toggle="collapse" data-target="#collapseFaskes"
                    aria-expanded="true" aria-controls="collapseFaskes">
                    <i class="fas fa-clinic-medical"></i>
                    <span>Data Faskes</span>
                </a>
                <div id="collapseFaskes" class="collapse {{ $dataNav['level1'] == "faskes" ? "show" : "" }}" aria-labelledby="headingUtilities"
                    data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <a class="collapse-item {{ $dataNav['level2'] == "faskes.lihat" ? "active" : "" }}" href="{{ url('faskes/lihat'); }}">Lihat Faskes</a>
                        <a class="collapse-item {{ $dataNav['level2'] == "faskes.data-master-poli" ? "active" : "" }}" href="{{ url('faskes/data-master-poli'); }}">Data Master Poli</a>
                    </div>
                </div>
            </li>
            @endif

            <!-- Nav Item - Utilities Collapse Menu -->
            <li class="nav-item {{ $dataNav['level1'] == "tindakan" ? "active" : "" }}">
                <a class="nav-link {{ $dataNav['level1'] == "tindakan" ? "" : "collapsed" }}" href="#" data-toggle="collapse" data-target="#collapseTindakan"
                    aria-expanded="true" aria-controls="collapseUtilities">
                    <i class="fas fa-laptop-medical"></i>
                    <span>Tindakan</span>
                </a>
                <div id="collapseTindakan" class="collapse {{ $dataNav['level1'] == "tindakan" ? "show" : "" }}" aria-labelledby="headingTindakan"
                    data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        @if(Auth::user()->role == "superadmin")
                        <a class="collapse-item {{ $dataNav['level2'] == "tindakan.verifikasi" ? "active" : "" }}" href="{{ url('tindakan/verifikasi'); }}">Verifikasi Tindakan</a>
                        @endif
                        <a class="collapse-item {{ $dataNav['level2'] == "tindakan.lihat" ? "active" : "" }}" href="{{ url('tindakan/lihat'); }}">Lihat Tindakan</a>
                    </div>
                </div>
            </li>

            <!-- Nav Item - Utilities Collapse Menu -->
            @if(Auth::user()->role == "superadmin")
            <li class="nav-item {{ $dataNav['level1'] == "news" ? "active" : "" }}">
                <a class="nav-link {{ $dataNav['level1'] == "news" ? "" : "collapsed" }}" href="#" data-toggle="collapse" data-target="#collapseNews"
                    aria-expanded="true" aria-controls="collapseNews">
                    <i class="fas fa-rss"></i>
                    <span>Data News</span>
                </a>
                <div id="collapseNews" class="collapse {{ $dataNav['level1'] == "news" ? "show" : "" }}" aria-labelledby="headingUtilities"
                    data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <a class="collapse-item {{ $dataNav['level2'] == "news.lihat" ? "active" : "" }}" href="{{ url('news/lihat'); }}">Lihat News</a>
                    </div>
                </div>
            </li>
            @endif

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>

        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                    <!-- Sidebar Toggle (Topbar) -->
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>

                    <!-- Topbar Search -->
                    {{-- <form
                        class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
                        <div class="input-group">
                            <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..."
                                aria-label="Search" aria-describedby="basic-addon2">
                            <div class="input-group-append">
                                <button class="btn btn-primary" type="button">
                                    <i class="fas fa-search fa-sm"></i>
                                </button>
                            </div>
                        </div>
                    </form> --}}

                    <!-- Topbar Navbar -->
                    <ul class="navbar-nav ml-auto">

                        <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                        <li class="nav-item dropdown no-arrow d-sm-none">
                            <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-search fa-fw"></i>
                            </a>
                            <!-- Dropdown - Messages -->
                            <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in"
                                aria-labelledby="searchDropdown">
                                <form class="form-inline mr-auto w-100 navbar-search">
                                    <div class="input-group">
                                        <input type="text" class="form-control bg-light border-0 small"
                                            placeholder="Search for..." aria-label="Search"
                                            aria-describedby="basic-addon2">
                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="button">
                                                <i class="fas fa-search fa-sm"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </li>

                        <!-- Nav Item - Alerts -->
                        {{-- <li class="nav-item dropdown no-arrow mx-1">
                            <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-bell fa-fw"></i>
                                <!-- Counter - Alerts -->
                                <span class="badge badge-danger badge-counter">3+</span>
                            </a>
                            <!-- Dropdown - Alerts -->
                            <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in"
                                aria-labelledby="alertsDropdown">
                                <h6 class="dropdown-header">
                                    Alerts Center
                                </h6>
                                <a class="dropdown-item d-flex align-items-center" href="#">
                                    <div class="mr-3">
                                        <div class="icon-circle bg-primary">
                                            <i class="fas fa-file-alt text-white"></i>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="small text-gray-500">December 12, 2019</div>
                                        <span class="font-weight-bold">A new monthly report is ready to download!</span>
                                    </div>
                                </a>
                                <a class="dropdown-item d-flex align-items-center" href="#">
                                    <div class="mr-3">
                                        <div class="icon-circle bg-success">
                                            <i class="fas fa-donate text-white"></i>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="small text-gray-500">December 7, 2019</div>
                                        $290.29 has been deposited into your account!
                                    </div>
                                </a>
                                <a class="dropdown-item d-flex align-items-center" href="#">
                                    <div class="mr-3">
                                        <div class="icon-circle bg-warning">
                                            <i class="fas fa-exclamation-triangle text-white"></i>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="small text-gray-500">December 2, 2019</div>
                                        Spending Alert: We've noticed unusually high spending for your account.
                                    </div>
                                </a>
                                <a class="dropdown-item text-center small text-gray-500" href="#">Show All Alerts</a>
                            </div>
                        </li> --}}

                        <!-- Nav Item - Messages -->
                        {{-- <li class="nav-item dropdown no-arrow mx-1">
                            <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-envelope fa-fw"></i>
                                <!-- Counter - Messages -->
                                <span class="badge badge-danger badge-counter">7</span>
                            </a>
                            <!-- Dropdown - Messages -->
                            <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in"
                                aria-labelledby="messagesDropdown">
                                <h6 class="dropdown-header">
                                    Message Center
                                </h6>
                                <a class="dropdown-item d-flex align-items-center" href="#">
                                    <div class="dropdown-list-image mr-3">
                                        <img class="rounded-circle" src="img/undraw_profile_1.svg"
                                            alt="...">
                                        <div class="status-indicator bg-success"></div>
                                    </div>
                                    <div class="font-weight-bold">
                                        <div class="text-truncate">Hi there! I am wondering if you can help me with a
                                            problem I've been having.</div>
                                        <div class="small text-gray-500">Emily Fowler · 58m</div>
                                    </div>
                                </a>
                                <a class="dropdown-item d-flex align-items-center" href="#">
                                    <div class="dropdown-list-image mr-3">
                                        <img class="rounded-circle" src="img/undraw_profile_2.svg"
                                            alt="...">
                                        <div class="status-indicator"></div>
                                    </div>
                                    <div>
                                        <div class="text-truncate">I have the photos that you ordered last month, how
                                            would you like them sent to you?</div>
                                        <div class="small text-gray-500">Jae Chun · 1d</div>
                                    </div>
                                </a>
                                <a class="dropdown-item d-flex align-items-center" href="#">
                                    <div class="dropdown-list-image mr-3">
                                        <img class="rounded-circle" src="img/undraw_profile_3.svg"
                                            alt="...">
                                        <div class="status-indicator bg-warning"></div>
                                    </div>
                                    <div>
                                        <div class="text-truncate">Last month's report looks great, I am very happy with
                                            the progress so far, keep up the good work!</div>
                                        <div class="small text-gray-500">Morgan Alvarez · 2d</div>
                                    </div>
                                </a>
                                <a class="dropdown-item d-flex align-items-center" href="#">
                                    <div class="dropdown-list-image mr-3">
                                        <img class="rounded-circle" src="https://source.unsplash.com/Mv9hjnEUHR4/60x60"
                                            alt="...">
                                        <div class="status-indicator bg-success"></div>
                                    </div>
                                    <div>
                                        <div class="text-truncate">Am I a good boy? The reason I ask is because someone
                                            told me that people say this to all dogs, even if they aren't good...</div>
                                        <div class="small text-gray-500">Chicken the Dog · 2w</div>
                                    </div>
                                </a>
                                <a class="dropdown-item text-center small text-gray-500" href="#">Read More Messages</a>
                            </div>
                        </li> --}}

                        {{-- <div class="topbar-divider d-none d-sm-block"></div> --}}

                        <!-- Nav Item - User Information -->
                        <li class="nav-item dropdown no-arrow">
                            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                @if(auth()->user()->role == "faskes")
                                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><strong>{{ auth()->user()->nama }}</strong> | {{ auth()->user()->role }} - {{ Session::get('satker'); }}</span>
                                @else
                                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><strong>{{ auth()->user()->nama }}</strong> | {{ auth()->user()->role }}</span>
                                @endif
                                <img class="img-profile rounded-circle"
                                    src="{{ asset('img/undraw_profile.svg') }}">
                            </a>
                            <!-- Dropdown - User Information -->
                            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                                aria-labelledby="userDropdown">
                                <a class="dropdown-item" href="#">
                                    <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Profil
                                </a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Logout
                                </a>
                            </div>
                        </li>

                    </ul>

                </nav>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    @yield('content')

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; TERATAI KESJAS {{ date('Y')}}</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Konfirmasi Logout</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Apakah Anda yakin akan logout?</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                    <a class="btn btn-primary" href="{{ url('/logout') }}">Logout</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Toast For All -->
    <div class="position-fixed bottom-0 right-0 p-3" style="z-index: 9999; right: 0; top: 0;">
        <div id="liveToast" class="toast" role="alert" aria-live="assertive" aria-atomic="true" data-delay="5000">
            <div id="toast-color" class="toast-header">
                <strong class="mr-auto text-white" id="toast-message"></strong>
                <button type="button" class="ml-2 mb-1 close text-white" data-dismiss="toast" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="toast-body" id="toast-sub-message"></div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{ asset('js/sb-admin-2.min.js') }}"></script>

    <!-- Page spesific js -->
    @yield('js-page-spesific-plugin')

    <!-- Page custom js -->
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function showToast(color, message, subMessage) {
            $("#toast-color").attr('class', 'toast-header bg-'+color);
            $('#toast-message').html(message);
            $('#toast-sub-message').html(subMessage);
            $('#liveToast').toast('show');
        }
    </script>
    @yield('js-page-custom')

</body>

</html>