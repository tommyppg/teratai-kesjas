<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>TERATAI KESJAS - Privacy Policy</title>

    <link rel="icon" type="image/x-icon" href="{{ asset('img/favicon.ico') }}">

    <!-- Custom fonts for this template-->
    <link href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="{{ asset('css/sb-admin-2.css') }}" rel="stylesheet">

</head>

<body class="bg-gradient-primary">

    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center">

            <div class="col-xl-10 col-lg-12 col-md-9">

                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            {{-- <div class="col-lg-6 d-none d-lg-block bg-login-image"></div> --}}
                            <div class="col-lg-12">
                                <div class="p-5">
                                    <div class="text-center">
                                        <img style="margin-bottom: 15px !important;" src="{{ asset('img/logo-brimob.png') }}" width="100px">
                                        <img style="margin-bottom: 15px !important;" src="{{ asset('img/logo-brimob-u-indonesia.png') }}" width="150px">
                                        <h1 class="h4 text-gray-900 mb-4"><strong>Privacy Policy</strong></h1>
                                        <p style="margin-top: -22px !important;">Ketentuan Layanan <br> Kebijakan Privasi Teratai Kesjas</p>
                                        <hr>
                                    </div>
                                    <div style="text-align: justify !important;">
                                        <h4 class="text-gray-900">Kebijakan Privasi</h4>
                                        <p>
                                            MOHON ANDA MEMBACA KEBIJAKAN PRIVASI INI DENGAN SEKSAMA UNTUK MEMASTIKAN BAHWA ANDA MEMAHAMI BAGAIMANA TERATAI KESJAS MENGUMPULKAN, MENYIMPAN, MENGGUNAKAN, MEMINDAHKAN, MENGUNGKAPKAN DAN MELINDUNGI INFORMASI PRIBADI YANG DIPEROLEH MELALUI APLIKASI KAMI.
                                            
                                            Penggunaan Anda atas Aplikasi dan Layanan kami tunduk pada Ketentuan Penggunaan dan Kebijakan Privasi ini dan mengindikasikan persetujuan Anda terhadap Ketentuan Penggunaan dan Kebijakan Privasi tersebut.
                                        </p>

                                        <h4 class="text-gray-900">Definisi</h4>
                                        <p>
                                            "Aplikasi" berarti aplikasi Teratai Kesjas yang tersedia di website, Google Play Store, App Store maupun store yang tersedia dalam sistem perangkat Anda.
                                        </p>
                                        <p>
                                            "Informasi Pribadi" berarti data perseorangan/perusahaan tertentu yang melekat dan dapat diidentifikasi pada suatu individu/perusahaan dan yang dikumpulkan melalui Aplikasi, seperti nama, alamat, nomor identitas (apabila Anda adalah seorang individu), data dan dokumen identitas perusahaan (apabila Anda bukan seorang individu), nomor telepon, alamat surat elektronik (e-mail), nomor rekening bank, perizinan dan/atau sejenisnya, dan informasi lain yang mungkin dapat mengidentifikasi Anda sebagai pengguna Aplikasi.
                                        </p>
                                        <p>
                                            "Teratai Kesjas" adalah aplikasi yang dikelola oleh Brimob Indonesia.
                                        </p>
                                        <p>
                                            "Ketentuan Penggunaan" berarti syarat dan ketentuan atau prosedur standar operasi atau ketentuan lainnya sehubungan dengan masing-masing Aplikasi yang dikembangkan oleh TaniHub, sebagaimana dapat diubah atau ditambah dari waktu ke waktu;
                                        </p>
                                        <p>
                                            "Layanan" berarti hal-hal yang ditawarkan oleh Teratai Kesjas melalui Aplikasi kepada Anda, termasuk namun tidak terbatas pada pemrosesan dan pengantaran untuk produk yang disediakan dari waktu ke waktu melalui Aplikasi.
                                        </p>

                                        <h4 class="text-gray-900">Tujuan Informasi Pribadi yang Teratai Kesjas Kumpulkan</h4>
                                        <p>
                                            Teratai Kesjas mengumpulkan Informasi Pribadi tertentu dari Anda agar Aplikasi dapat menjalankan fungsinya namun tidak terbatas dengan tujuan untuk menghubungi Anda tentang akun Anda dalam Aplikasi dan Layanan Teratai Kesjas, memberikan Layanan kepada Anda, menjawab pertanyaan-pertanyaan dan/atau permintaan-permintaan yang Anda ajukan kepada Teratai Kesjas, dan untuk mendeteksi, mencegah, mengurangi dan menyelidiki aktivitas curang atau ilegal.
                                        </p>

                                        <h4 class="text-gray-900">Pemberian Informasi Pribadi oleh Anda</h4>
                                        <p>
                                            Informasi Pribadi dapat Anda berikan secara langsung (sebagai contoh, saat Anda mendaftar sebagai pengguna Aplikasi) maupun terkumpul ketika Anda menggunakan Aplikasi.
                                        </p>

                                        <h4 class="text-gray-900">Perlindungan Data dan Informasi Pribadi</h4>
                                        <p>
                                            Perlindungan data dan Informasi Pribadi Anda adalah suatu kewajiban bagi Teratai Kesjas. Teratai Kesjas akan memberlakukan langkah-langkah untuk melindungi dan mengamankan data dan Informasi Pribadi Anda. Namun demikian, Teratai Kesjas tidak dapat sepenuhnya menjamin bahwa sistem Teratai Kesjas tidak akan diakses soleh virus, malware, gangguan atau kejadian luar biasa termasuk akses oleh pihak ketiga yang tidak berwenang. Anda harus menjaga keamanan dan kerahasiaan data yang berkaitan dengan akun Anda pada Aplikasi termasuk kata sandi dan data-data lainnya yang Anda berikan di dalam Aplikasi ini.
                                        </p>

                                        <h4 class="text-gray-900">Perubahan atas Kebijakan Privasi ini</h4>
                                        <p>
                                            Teratai Kesjas dapat mengubah Kebijakan Privasi ini untuk sejalan dengan perkembangan kegiatan bisnis Teratai Kesjas atau dipersyaratkan oleh peraturan perundang-undangan dan instusi pemerintah terkait. Jika Teratai Kesjas mengubah Kebijakan Privasi ini, Teratai Kesjas akan memberitahu Anda melalui  notifikasi pada Aplikasi 1 (satu) hari sebelum perubahan berlaku. Teratai Kesjas meminta Anda untuk meninjau Aplikasi secara reguler dan terus-menerus selama Anda menggunakan Aplikasi untuk mengetahui informasi terbaru tentang bagaimana ketentuan Kebijakan Privasi ini Teratai Kesjas diberlakukan.
                                        </p>

                                        <h4 class="text-gray-900">Pengakuan dan Persetujuan</h4>
                                        <p>
                                            Dengan menggunakan Aplikasi, Anda mengakui bahwa Anda telah membaca dan memahami Kebijakan Privasi ini dan Ketentuan Penggunaan dan setuju dan sepakat terhadap penyimpanan, penggunaan, pemrosesan, pemberian dan pengalihan Informasi Pribadi Anda oleh Teratai Kesjas sebagaimana dinyatakan di dalam Kebijakan Privasi ini.
                                        </p>
                                        <p>
                                            Teratai Kesjas juga menyatakan bahwa Anda memiliki hak untuk memberikan seluruh informasi yang telah Anda berikan kepada Teratai Kesjas dan untuk memberikan hak kepada Teratai Kesjas untuk memproses dan menggunakan serta memberikan informasi guna mendukung dan melaksanakan fungsi Aplikasi dan membebaskan Teratai Kesjas dari segala tuntutan apabila di kemudian hari terdapat tuntutan ketidakabsahan hak Anda dalam memberikan informasi.
                                        </p>

                                        <h4 class="text-gray-900">Cara untuk Menghubungi Teratai Kesjas</h4>
                                        <p>
                                            Jika Anda memiliki pertanyaan lebih lanjut tentang privasi dan keamanan informasi Anda atau ingin memperbarui atau menghapus data Anda maka silakan hubungi kami di: <a href="mailto:terataikesjas@gmail.com">terataikesjas@gmail.com</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendor/jquery-validation-1.19.5/dist/jquery.validate.js'); }}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{ asset('js/sb-admin-2.min.js') }}"></script>

    <script>
        $( document ).ready( function () {
            $( "#sign-in" ).validate( {
				rules: {
					email: {
						required: true,
						email: true
					},
					password: {
						required: true
					},
				},
				messages: {
                    email: {
						required: "Mohon masukkan email Anda",
						email: "Format email tidak sesuai"
					},
					password: {
						required: "Mohon masukkan password Anda"
					},
				},
				errorElement: "div",
                errorClass: 'invalid-feedback d-block',
				errorPlacement: function ( error, element ) {
					if ( element.prop( "type" ) === "checkbox" ) {
						error.insertAfter( element.parent( "label" ) );
					} else {
						error.insertAfter( element );
					}
				},
				highlight: function ( element, errorClass, validClass ) {
					$( element ).removeClass( "is-valid" );
				},
				unhighlight: function (element, errorClass, validClass) {
					$( element ).addClass( "is-valid" );
				},
                submitHandler: function (form) {
                    $.ajax({
                        type: "POST",
                        url: $('#sign-in').attr('action'),
                        data: $('#sign-in').serialize(),
                        beforeSend: function(){
                            //change button condition
                            $('#btn-submit').prop('disabled', true);
                            $('#btn-submit').html('Loading...');

                            //get rid alert
                            $('#alert-login-failed').addClass('d-none');
                            $('#alert-login-failed').removeClass('d-block');
                        },
                        success: function (result) {
                            if(result.success){
                                window.location.href = "{{ url('/') }}";
                            }else{
                                $('#alert-login-failed').removeClass('d-none');
                                $('#alert-login-failed').addClass('d-block');
                            }

                            $('#btn-submit').prop('disabled', false);
                            $('#btn-submit').html('Login');
                        }
                    });
                    return false; // required to block normal submit since you used ajax
                }
			} );
        });
    </script>

</body>

</html>