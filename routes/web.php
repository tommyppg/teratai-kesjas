<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\AuthController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\RikkesController;
use App\Http\Controllers\TKJController;
use App\Http\Controllers\AnggotaController;
use App\Http\Controllers\TindakanController;
use App\Http\Controllers\FaskesController;
use App\Http\Controllers\PenyakitController;
use App\Http\Controllers\NewsController;
use App\Http\Controllers\PrivacyPolicyController;
use App\Http\Controllers\ContactUsController;

Route::get('/', [DashboardController::class, 'index'])->middleware('auth');
Route::get('/dashboard/calculate-kelainan', [DashboardController::class, 'calculateKelainan'])->middleware('auth');

Route::middleware('auth')->group(function () {
    Route::prefix('rikkes')->group(function () {
        Route::get('/import', [RikkesController::class, 'index']);
        Route::post('/import', [RikkesController::class, 'import']);
        Route::get('/lihat', [RikkesController::class, 'lihat']);
        Route::get('/get-data', [RikkesController::class, 'getData']);
        Route::get('/detail/{nrp}', [RikkesController::class, 'detail']);
        Route::get('/sudah-periksa', [RikkesController::class, 'sudahPeriksa']);
        Route::get('/get-data-sudah-periksa', [RikkesController::class, 'getDataSudahPeriksa']);
    });

    Route::prefix('tkj')->group(function () {
        Route::get('/import', [TKJController::class, 'index']);
        Route::post('/import', [TKJController::class, 'import']);
        Route::get('/lihat', [TKJController::class, 'lihat']);
        Route::get('/get-data', [TKJController::class, 'getData']);
        Route::get('/detail/{nrp}', [TKJController::class, 'detail']);
    });

    Route::prefix('penyakit')->group(function () {
        Route::get('/lihat', [PenyakitController::class, 'index']);
        Route::get('/detail/{id_kategori_penyakit}', [PenyakitController::class, 'detail']);
        Route::get('/get-data-monitoring', [PenyakitController::class, 'getDataMonitoring']);
        Route::get('/get-data-detail-monitoring/{id_kategori_penyakit}', [PenyakitController::class, 'getDataDetailMonitoring']);
        
        Route::get('/data-master', [PenyakitController::class, 'dataMaster']);
        Route::get('/get-data-master-kategori-penyakit', [PenyakitController::class, 'getDataMasterKategoriPenyakit']);
        Route::post('/add-or-update-kategori-penyakit', [PenyakitController::class, 'addOrUpdateKategoriPenyakit']);
        Route::get('/get-kategori-penyakit', [PenyakitController::class, 'getKategoriPenyakit']);
        Route::get('/delete-kategori-penyakit', [PenyakitController::class, 'deleteKategoriPenyakit']);

        
        Route::get('/data-master-daftar-kelainan/{id_kategori_penyakit}', [PenyakitController::class, 'dataMasterDaftarKelainan']);
        Route::get('/get-data-master-daftar-kelainan/{id_kategori_penyakit}', [PenyakitController::class, 'getDataMasterDaftarKelainan']);
        Route::post('/add-or-update-daftar-kelainan', [PenyakitController::class, 'addOrUpdateDaftarKelainan']);
        Route::get('/get-daftar-kelainan', [PenyakitController::class, 'getDaftarKelainan']);
        Route::get('/delete-daftar-kelainan', [PenyakitController::class, 'deleteDaftarKelainan']);
    });

    Route::prefix('anggota')->group(function () {
        Route::get('/lihat', [AnggotaController::class, 'lihat']);
        Route::get('/get-data', [AnggotaController::class, 'getData']);
        Route::get('/detail/{nrp}', [AnggotaController::class, 'detail']);
        Route::get('/detail/{nrp}', [AnggotaController::class, 'detail']);
        Route::get('/change-status-user', [AnggotaController::class, 'changeStatusUser']);
    });

    Route::prefix('faskes')->group(function () {
        Route::get('/lihat', [FaskesController::class, 'lihat']);
        Route::post('/add-or-update-faskes', [FaskesController::class, 'addOrUpdateFaskes']);
        Route::get('/get-data-faskes', [FaskesController::class, 'getDataFaskes']);
        Route::get('/get-data-faskes-poli/{id_satker}', [FaskesController::class, 'getDataFaskesPoli']);
        Route::get('/get-faskes', [FaskesController::class, 'getFaskes']);
        Route::get('/delete-faskes', [FaskesController::class, 'deleteFaskes']);
        Route::get('/detail/{id}', [FaskesController::class, 'detail']);
        Route::get('/detail-poli/{id}', [FaskesController::class, 'detailPoli']);
        Route::get('/get-data-user/{id}', [FaskesController::class, 'getDataUser']);
        Route::post('/add-or-update-user', [FaskesController::class, 'addOrUpdateUser']);
        Route::get('/get-user', [FaskesController::class, 'getUser']);
        Route::get('/delete-user', [FaskesController::class, 'deleteUser']);
        Route::get('/email', [FaskesController::class, 'email']);

        Route::get('/data-master-poli', [FaskesController::class, 'dataMasterPoli']);
        Route::get('/get-data-master-jenis-poli', [FaskesController::class, 'getDataMasterJenisPoli']);
        Route::get('/get-jenis-poli', [FaskesController::class, 'getJenisPoli']);
        Route::post('/add-or-update-jenis-poli', [FaskesController::class, 'addOrUpdateJenisPoli']);
        Route::get('/delete-jenis-poli', [FaskesController::class, 'deleteJenisPoli']);
    });

    Route::prefix('tindakan')->group(function () {
        Route::get('/verifikasi', [TindakanController::class, 'verifikasi']);
        Route::post('/verifikasi-process', [TindakanController::class, 'verifikasiProcess']);
        Route::get('/get-data-verifikasi', [TindakanController::class, 'getDataVerifikasi']);
        Route::get('/lihat', [TindakanController::class, 'lihat']);
        Route::get('/get-data-anggota', [TindakanController::class, 'getDataAnggota']);
        Route::get('/detail/{nrp}', [TindakanController::class, 'detail']);
        Route::get('/detail-tindakan/{id_rikkes}', [TindakanController::class, 'detailTindakan']);
        Route::get('/get-data-rikkes/{nrp}', [TindakanController::class, 'getDataRikkes']);
        Route::get('/get-data-tkj/{nrp}', [TindakanController::class, 'getDataTKJ']);
        Route::get('/get-data-tindakan/{id_rikkes}', [TindakanController::class, 'getDataTindakan']);
        Route::get('/get-tindakan', [TindakanController::class, 'getTindakan']);
        Route::get('/delete-tindakan', [TindakanController::class, 'deleteTindakan']);
        Route::post('/add-or-update-tindakan', [TindakanController::class, 'addOrUpdateTindakan']);
    });

    Route::prefix('news')->group(function () {
        Route::get('/lihat', [NewsController::class, 'lihat']);
        Route::get('/get-data-news', [NewsController::class, 'getDataNews']);
        Route::post('/add-or-update-news', [NewsController::class, 'addOrUpdateNews']);
        Route::get('/get-news', [NewsController::class, 'getNews']);
        Route::get('/delete-news', [NewsController::class, 'deleteNews']);
    });
});

Route::get('news_file/{filename}', function ($filename)
{
    $path = storage_path('app/public/files/news/' . $filename);

    if (!File::exists($path)) {
        abort(404);
    }

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});

Route::get('/login', [AuthController::class, 'index'])->name('login');
Route::post('/login', [AuthController::class, 'authenticate']);
Route::get('/logout', [AuthController::class, 'logout']);
Route::get('/set-new-password/{token}', [AuthController::class, 'setNewPassword']);
Route::post('/set-new-password', [AuthController::class, 'setNewPasswordProccess']);

Route::get('/privacy-policy', [PrivacyPolicyController::class, 'privacyPolicy']);
Route::get('/contact-us', [ContactUsController::class, 'contactUs']);

Route::get('/clear_cache', function () {
    \Artisan::call('config:cache');
    \Artisan::call('view:clear');
    \Artisan::call('route:clear');
    dd("Cache is cleared");
});

// Route::get('/kirimemail', [DashboardController::class, 'kirimemail']);